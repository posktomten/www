<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="../css/stilmall.css">
   
    <script src="../../js/highlight.min.js"></script>
    <script src="../../jquery/jquery.js"></script> 
    <script>
        hljs.highlightAll();
    </script>
<script>
function atomonedark() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">');
}

function atomonelight() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-light.min.css">');
}
    </script>

<title>Build AppImage - The code</title>
</head>
<body class="code">
           <section>
           <h3>build_appimage.sh &nbsp; | &nbsp; <span class="highlight" onclick="atomonelight()">Light</span>&nbsp; | &nbsp;<span class="highlight" onclick="atomonedark()">Dark</span>&nbsp; | &nbsp;<span class="highlight" onclick="window.close()">Close</span></h3>
            <pre><code>         
<?php
$file = file_get_contents("build64_latest.bat");
$file_cleaned = htmlspecialchars($file, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5);
echo $file_cleaned;
?>
</code></pre>
</section>
     

</body>
</html>