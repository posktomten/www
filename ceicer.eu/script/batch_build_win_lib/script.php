<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="../css/stilmall.css">
   
    <script src="../../js/highlight.min.js"></script>
    <script src="../../jquery/jquery.js"></script>
    <script>
        hljs.highlightAll();
    </script>

    <script>
        $(document).ready(

            function () {
                $("img").dblclick(function () {
                    this.requestFullscreen()
                })
            }

        );

            function copyToClipboard(toclipboard) {
                        var range = document.createRange();
                        range.selectNode(document.getElementById(toclipboard));
                        window.getSelection().removeAllRanges(); // clear current selection
                        window.getSelection().addRange(range); // to select text
                        document.execCommand("copy");
                        window.getSelection().removeAllRanges();// to deselect
                        alert("The contents of the file have been copied to the clipboard.");
                    }

function atomonedark() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">');
}

function atomonelight() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-light.min.css">');
}
    </script>

    <title>Unnecessary files</title>
</head>

<div class="container">
    <header>
        <p>Windows Qt lib build script</p>


    </header>
    <nav>
        <div class="btn-group">
            <a class="button" href="../../index.php">ceicer.eu</a>



        </div> <!-- btn-group -->
    </nav>
    <div class="content">
        <!-- content 1 -->
        <section class="smal">

            <h3>Scripts to simplify the compilation of Qt programs or libraries.</h3>
            <p>MinGW GCC</p>

            <br>
            <h3>Example (Double click for full screen)</h3>
            <img src="images/mingw.png" alt="MinGW GGC">


   
                    <h3>Download script</h3> 
                    <p><a href="../../bin/script/build64_latest.bat.zip">build64_latest.bat.zip</a></p>

        </section>


        <section class="big">

        <h3>build64_latest.bat &nbsp; | &nbsp; <span class="highlight" onclick="atomonelight()">Light</span>&nbsp; | &nbsp;<span class="highlight" onclick="atomonedark()">Dark</span> &nbsp; | &nbsp;<span class="highlight"><a href="codemingw.php" target="_blank">Open in new tab</a></span>&nbsp; | &nbsp; <span class="highlight" onclick="copyToClipboard('toclipboardmingw')">Copy to clipboard</span></h3>




<pre><code id="toclipboardmingw">         
<?php
$file = file_get_contents("build64_latest.bat");
$file_cleaned = htmlspecialchars($file, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5);
echo $file_cleaned;

?>
</code></pre>
        </section>

        <!-- Article 1 -->
        <!--
        <article>
            <h3>Heightlighted code</h3>
            <p>Heightlighted code using css and script from <a href="https://highlightjs.org/"
                    target="_blank">https://highlightjs.org</a>

        </article>
    -->
    </div> <!-- content 1 -->



    <!-- content 2 -->
    
    <div class="content">
    <section class="smal">

    <h3>Scripts to simplify the compilation of Qt programs or libraries.</h3>

    <p>Microsoft Visual Studio</p>
    <br>
        <h3>Example (Double click for full screen)</h3>
       <img src="images/msvc.png" alt="Microsoft Visual Studio">

       <h3>Download script</h3> 
                    <p><a href="../../bin/script/build64_latest_msvc.bat.zip">build64_latest_msvc.bat.zip</a></p>
        </section>


        <section class="big">

        <h3>build64_latest_msvc.bat &nbsp; | &nbsp; <span class="highlight" onclick="atomonelight()">Light</span>&nbsp; | &nbsp;<span class="highlight" onclick="atomonedark()">Dark</span> &nbsp; | &nbsp;<span class="highlight"><a href="codemsvc.php" target="_blank">Open in new tab</a></span>&nbsp; | &nbsp; <span class="highlight" onclick="copyToClipboard('toclipboardmsvc')">Copy to clipboard</span></h3>




<pre><code id="toclipboardmsvc">         
<?php
$file = file_get_contents("build64_latest_msvc.bat");
$file_cleaned = htmlspecialchars($file, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5);
echo $file_cleaned;

?>
</code></pre>
        </section>

<!--
       <article>
       content 2, article

      </article>
-->

    </div> 

    <!-- content 2 -->
    <footer>
    <div>&copy; Copyright 2023 - <?php echo date("Y"); ?>
            Ingemar Ceicer<br>
            programming@ceicer.com
        </div>
    </footer>

</div> <!-- container -->

</body>

</html>