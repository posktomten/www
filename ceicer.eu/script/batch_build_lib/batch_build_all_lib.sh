#!/bin/bash

# Compile multiple Qt programs or libraries with one command


# batch_build_all_lib.sh Copyright (C) 2023 Ingemar Ceicer
# https://ceicer.eu
# programming@ceicer.com

# batch_build_all_lib.sh is free software:
# you can redistribute it and/or modify
# it under the terms of the
# GNU General Public License as published by
# the Free Software Foundation, version 3.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

if [ `getconf LONG_BIT` == 64 ]; then
	GCC="gcc_64"
else 
	GCC="gcc_32"
fi

# EDIT
latestqt5=5.15.10
latestqt6=6.5.2
# END EDIT

declare -a libsource=("/home/ingemar/PROGRAMMERING/downloadunpack/bibliotek/code"
    "/home/ingemar/PROGRAMMERING/libcheckupdate/bibliotek/code"
    "/home/ingemar/PROGRAMMERING/libselectfont/code"
    "/home/ingemar/PROGRAMMERING/libzsyncupdateappimage/bibliotek/code"
    "/home/ingemar/PROGRAMMERING/libtexttospeech/bibliotek/code"
    "/home/ingemar/PROGRAMMERING/libcreateshortcut/bibliotek/code"
    "/home/ingemar/PROGRAMMERING/libabout/code")

GLIBC=$(glibcversion)
BIT=$(getconf LONG_BIT)

delete_old_lib() {
    PS3='Please enter your choice: '
    options=("Yes" "No")
    echo -------------------------------------------------------------
    echo "The library files exist. Do you want to delete them and compile new ones?"

    select opt in "${options[@]}"; do
        case $opt in
        "Yes")
            rm -r $1
            rm -r $2

            break
            ;;

        "No")
            exit
            break
            ;;
        *)
            echo "invalid option $REPLY"
            ;;
        esac
    done

}

HERE=$(pwd)

echo -------------------------------------------------------------
echo build these libraries

for i in "${libsource[@]}"; do
    echo "$i"
done
PS3='Please enter your choice: '
options=("5.15.2" "${latestqt5}" "${latestqt6}" "Qt6.4.1_static" "Quit")
echo -------------------------------------------------------------
select opt in "${options[@]}"; do
    case $opt in
    "5.15.2")
        DEBUG_LIB=1
        echo "You chose choice $REPLY which is $opt"
        qmakePath="/opt/Qt/5.15.2/${GCC}/bin/qmake"
        LD_LIBRARY_PATH="/opt/Qt/5.15.2/${GCC}/lib${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}"
        PATH="/opt/Qt/5.15.2/${GCC}/bin${PATH:+:${PATH}}"
        lib=$HERE/lib_Qt5.15.2_GLIBC-${GLIBC}_${BIT}-bit
        lib_folder=lib_Qt5.15.2_GLIBC-${GLIBC}_${BIT}-bit
        lib_release=$HERE/lib_Qt5.15.2_GLIBC-${GLIBC}_${BIT}-bit_release
        if [ -d "$lib" ]; then
            delete_old_lib $lib_folder $lib_release
        fi
        mkdir "$lib"
        mkdir "$lib_release"

        break
        ;;

    "${latestqt5}")
        DEBUG_LIB=0
        echo "You chose choice $REPLY which is $opt"
        qmakePath="/opt/Qt/${latestqt5}/${GCC}/bin/qmake"
        LD_LIBRARY_PATH="/opt/Qt/${latestqt5}/${GCC}/lib:${LD_LIBRARY_PATH}"
        PATH="/opt/Qt/${latestqt5}/${GCC}/bin:${PATH}"
        lib=$HERE/lib_Qt${latestqt5}_GLIBC-${GLIBC}_${BIT}-bit
        lib_folder=lib_Qt${latestqt5}_GLIBC-${GLIBC}_${BIT}-bit
        #lib_release=$HERE/lib_Qt${latestqt5}_GLIBC-${GLIBC}_${BIT}-bit_release
        if [ -d "$lib" ]; then
            delete_old_lib $lib_folder $lib_release
        fi
        mkdir "$lib"
        #mkdir "$lib_release"
        break
        ;;

    "${latestqt6}")
        DEBUG_LIB=1
        echo "You chose choice $REPLY which is $opt"
        qmakePath="/opt/Qt/${latestqt6}/${GCC}/bin/qmake"
        LD_LIBRARY_PATH="/opt/Qt/${latestqt6}/${GCC}/lib:${LD_LIBRARY_PATH}"
        PATH="/opt/Qt/${latestqt6}/${GCC}/bin:${PATH}"
        lib=$HERE/lib_Qt${latestqt6}_GLIBC-${GLIBC}_${BIT}-bit
        lib_folder=lib_Qt${latestqt6}_GLIBC-${GLIBC}_${BIT}-bit
        lib_release=$HERE/lib_Qt${latestqt6}_GLIBC-${GLIBC}_${BIT}-bit_release
        if [ -d "$lib" ]; then
            delete_old_lib $lib_folder $lib_release
        fi
        mkdir "$lib"
        mkdir "$lib_release"
        break
        ;;

    "Qt6.4.1_static")
        DEBUG_LIB=0
        echo "You chose choice $REPLY which is $opt"
        qmakePath="/opt/Qt/Qt6.4.1_static/bin/qmake"
        LD_LIBRARY_PATH="/opt/Qt/Qt6.4.1_static/lib:${LD_LIBRARY_PATH}"
        PATH="/opt/Qt/Qt6.4.1_static/bin:${PATH}"
        lib=$HERE/lib_Qt6.4.1_static_GLIBC-${GLIBC}_${BIT}-bit
        lib_folder=lib_Qt6.4.1_static_GLIBC-${GLIBC}_${BIT}-bit
        #lib_release=$HERE/lib_Qt6.4.1_static_GLIBC-${GLIBC}_${BIT}-bit_release
        if [ -d "$lib" ]; then
            delete_old_lib $lib_folder $lib_release
        fi
        mkdir "$lib"
        mkdir "$lib_release"
        break
        ;;
    "Quit")
        exit 0
        break
        ;;
    *) echo "invalid option $REPLY" ;;
    esac
done

for i in "${libsource[@]}"; do

    cd $HERE
    if [ -d build ]; then
        rm -r build
    fi
    if [ -d build-lib ]; then
        rm -r build-lib
    fi
    if [ -d code ]; then
        rm -r code
    fi

    mkdir code
    cp -r "$i"/* code/
    mkdir build
    cd build
    if [[ "$DEBUG_LIB" == 1 ]]; then
        $qmakePath -project "$i"/*.pro
        $qmakePath "$i"/*.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
        make -j$(nproc)
        rm -r *
    fi
    $qmakePath -project "$i"/*.pro
    $qmakePath "$i"/*.pro -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
    /usr/bin/make -j$(nproc)
    rm -r *

    cd ..
    rm -r code
    rm -r build

    cd build-lib
    cp -f * "$lib"
    if [[ "$DEBUG_LIB" == 1 ]]; then
        cp -f $(ls | egrep -v '^*.d.so') "$lib_release"
    fi
    cd ..
    rm -r build-lib

done

PS3='Please enter your choice: '
options=("Yes" "No")
echo -------------------------------------------------------------
echo "Do you want to upload the library files?"

select opt in "${options[@]}"; do
    case $opt in
    "Yes")

        cd "$HERE"

        date_now=$(date "+%FT%H-%M-%S")

        fil=${lib_folder}-${date_now}.tar.xz
        tar cf - "$lib_folder"/ | xz -z - >"$fil"
        #./md5_lib.sh "$fil"

        HOST="$(sed -n 1p ../../secret)"
        USER="$(sed -n 2p ../../secret)"
        PASSWORD="$(sed -n 3p ../../secret)"
        echo "$HOST"
        echo "$USER"
        echo "$PASSWORD"

        ftp -vp -inv $HOST <<EOF

user $USER $PASSWORD
cd "/bin.ceicer.com/public_html/compiled_lib
put $fil
put "$fil"-MD5.txt
bye
EOF

        PS3='Please enter your choice: '
        options=("Yes" "No")
        echo -------------------------------------------------------------
        echo "Do you want to remove the compressed file containing the library files?"
        select opt in "${options[@]}"; do
            case $opt in
            "Yes")
                rm $fil
                rm "$fil"-MD5.txt

                break
                ;;

            "No")
                break
                ;;
            *)
                echo "invalid option $REPLY"
                ;;
            esac
        done

        break
        ;;

    "No")
        break
        ;;
    *)
        echo "invalid option $REPLY"
        ;;
    esac

done

if [[ ${lib} == *"static"* ]]; then
    echo "STATIC Library Finished!"
    exit 0
fi

mkdir linked
cp ${lib}/*.so linked/
cd linked
for i in lib*.so; do
    ln -s "$i" "${i}.1"
    ln -s "$i" "${i}.1.0"
    ln -s "$i" "${i}.1.0.0"

done
cd ..
mv linked ${lib}_linked

if [ ! -d "$(pwd)/${lib_release}" ]; then

    mkdir linked
    cp ${lib_release}/*.so linked/
    cd linked
    for i in lib*.so; do
        ln -s "$i" "${i}.1"
        ln -s "$i" "${i}.1.0"
        ln -s "$i" "${i}.1.0.0"

    done
    cd ..
    mv linked ${lib_release}_linked

fi
echo "Finished!"





