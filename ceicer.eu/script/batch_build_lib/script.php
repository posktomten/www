<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/stilmall.css"> 
    <link rel="stylesheet" type="text/css" href="../css/stilmall.css">
   
    <script src="../../js/highlight.min.js"></script>
    <script src="../../jquery/jquery.js"></script>
    <script>
        hljs.highlightAll();
    </script>

    <script>
        $(document).ready(
            function () {
                $("img").dblclick(function () {
                    this.requestFullscreen()
                })
            }

        );

            function copyToClipboard() {
                            var range = document.createRange();
                            range.selectNode(document.getElementById("toclipboard"));
                            window.getSelection().removeAllRanges(); // clear current selection
                            window.getSelection().addRange(range); // to select text
                            document.execCommand("copy");
                            window.getSelection().removeAllRanges();// to deselect
                            alert("The contents of the file have been copied to the clipboard.");
                        }


function atomonedark() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">');
}

function atomonelight() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-light.min.css">');
}
    </script>

    <title>Unnecessary files</title>
</head>

<div class="container">
    <header>
        <p>BASH script Build all Qt lib</p>


    </header>
    <nav>
        <div class="btn-group">
            <a class="button" href="../../index.php">ceicer.eu</a>



        </div> <!-- btn-group -->
    </nav>
    <div class="content">
        <!-- content 1 -->
        <section class="smal">
            <h3> A bash script that allows you to compile multiple Qt programs or libraries with one command.</h3>

            <p>You must edit the script and enter all paths to yours *.pro files.
            </p><p>The script can also upload your libraries, compressed, to server using FTP. The filename contains the Qt version and a timestamp</p>
            <p>A small c++ program is used to check which GLIBC version is used. <a href="../build_appimage/script.php#glibc">glibc</a></p>

            <br>
            <h3>Step by step</h3>
            <figure>
                <img src="images/image1.png" alt="Step 1">
                <figcaption>
                    Step 1 
                </figcaption>
            </figure>
            <br>
            <figure>

                <img src="images/image2.png" alt="Step 2">
                <figcaption>
                    Step 2 
                </figcaption>
            </figure>
            <br>
            <figure>
                <img src="images/image3.png" alt="Step 3">
                <figcaption>
                    Step 3
                </figcaption>
            </figure>
            <br>
            <figure>
                <img src="images/image4.png" alt="Step 4">
                <figcaption>
                    Step 4
                </figcaption>
            </figure>
            <br>
            <figure>
                <img src="images/image5.png" alt="Step 5">
                <figcaption>
                    Step 5
                </figcaption>
            </figure>
          <br>
          <ol>
                    <li>lib_Qt6.5.2_GLIBC-2.35_64-bit
                        <ul>
                                <li>All compiled libraries. See the picture "All libraries"</li>
                        </ul></li>



                    <li>lib_Qt6.5.2_GLIBC-2.35_64-bit_linked
                        <ul>
                             <li>Same as 1, but now the libraries are linked using symbolic links (ln -s)</li>
                        </ul></li>


                    
                    <li>lib_Qt6.5.2_GLIBC-2.35_64-bit_release
                        <ul>
                                <li>Only the release version.</li>
                        </ul></li>


                    <li>lib_Qt6.5.2_GLIBC-2.35_64-bit_release_linked
                        <ul>
                            <li>Release versions, symbolic linked.</li>
                        </ul></li>



                    <li>lib_Qt6.5.2_GLIBC-2.35_64-bit-2023-09-15T21-41-37.tar.gz
                        <ul>
                               <li>All the library files compressed. The script can upload this file.</li>
                        </ul></li>
                </ol>


                <figure>
                    <img class="alllib" src="images/alllib.png" alt="All Libraries">
                    <figcaption>
                    All libraries
                    </figcaption>
                </figure>
    
           
          <br>

                    <h3>Download script</h3> 
                    <p><a href="../../bin/script/batch_build_all_lib.sh.tar.gz">batch_build_all_lib.sh.tar.gz</a></p>

        </section> 


        <section class="big">

        <h3>batch_build_all_lib.sh &nbsp; | &nbsp; <span class="highlight" onclick="atomonelight()">Light</span>&nbsp; | &nbsp;<span class="highlight" onclick="atomonedark()">Dark</span> &nbsp; | &nbsp;<span class="highlight"><a href="code.php" target="_blank">Open in new tab</a></span>&nbsp; | &nbsp; <span class="highlight" onclick="copyToClipboard()">Copy to clipboard</span></h3>



<pre><code id="toclipboard">          
<?php
$file = file_get_contents("batch_build_all_lib.sh");
$file_cleaned = htmlspecialchars($file, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5);
echo $file_cleaned;

?>
</code></pre>
        </section>

        <!-- Article 1 -->
        <!--
 

        </article>
    -->
    </div>  <!-- content 1 -->
    
   



    <!-- content 2 -->
    <!--
    <div class="content">
        <section>

        </section>

        <section>
        content 2, section 2
        
       </section>


       <article>
       content 2, article

      </article>


    </div> 
-->
    <!-- content 2 -->
    <footer>
    <div>&copy; Copyright 2023 - <?php echo date("Y"); ?>
            Ingemar Ceicer<br>
            programming@ceicer.com
        </div>
    </footer>

</div> <!-- container -->

</body>

</html>