<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">


    <meta name="keywords" content="posktomten, streamCapture2, streaming, download, television">
    <meta name="description" content="Download from SVTPlay and many other streaming service">
    <meta name="author" content="Ingemar Ceicer">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <script src="../jquery/jquery.js"></script>
    <title>streamCapture2</title>
    <script>
   $(document).ready(
      function(){
      $(".dbclick").dblclick(function(){
        this.requestFullscreen()
  }

)}

);
</script>
</head>
<body>
<div class="container">
<header>
    <img class="appicon" src="images/appicon.png" alt="streamCapture2 ikon">
        <p>streamCapture2</p>
       <!-- <img class="appicon" src="images/appicon.png" alt="streamCapture2 ikon"> -->

    </header>
    <nav>
        <div class="btn-group">
            <a class="button" href="../index.php">ceicer.eu</a>
            <a class="button" href="index.php">Sida 1</a>
            <a class="button" href="more.php">Sida 2</a>
            <a class="button" href="latest_sv.php">Ladda ner</a>
            <a class="button" href="http://bin.ceicer.com/streamcapture2/bin/" target="_blank">Ladda ner BETA</a>
            <a class="button" href=" https://bin.ceicer.com/streamcapture2/help-current/index_sv_SE.html" target="_blank">Manual</a>

            <a class="button" href="https://gitlab.com/posktomten/streamcapture2" target="_blank">Källkod</a>
            <a class="button" href="https://gitlab.com/posktomten/streamcapture2/-/wikis/home" target="_blank">Wiki (engelska)</a>
            <a class="button" href="https://gitlab.com/posktomten/streamcapture2/-/raw/master/LICENSE?ref_type=heads" target="_blank">Licens (engelska)</a>
            <a class="button" href="https://gitlab.com/posktomten/streamcapture2/-/raw/master/code/txt/CHANGELOG_sv_SE" target="_blank">Historik</a>


        </div> <!-- btn-group -->
    </nav>
    <div class="content">  <!-- content 1 -->
        <section>
        <h3> Ladda ner från streamingleverantörer</h3>
           <p><a href="https://svtplay-dl.se/" target="_blank">svtplay-dl</a> kan ladda ner media från olika "video on demand"-sidor.</p>
           <br>
            <p>streamCapture2 är ett skal för kommandoradsprogrammet svtplay-dl.

                Du väljer kvalitet och filstorlek. Programmet sparar dina sökningar. Ladda ner en fil i taget eller
                samla sökningar i en lista och ladda ner samtidigt. Programmet fungerar med Linux, Windows och MacOS X.
                Programmet är skrivet i C++ och använder Qt6 (Qt5 i versioner för äldre operativsystem) grafiska bibliotek. Det finns ett installationsprogram för
                Windows. Och <a href="https://appimage.org/" target="_blank">AppImage</a> för Linux (fungerar med alla Linux-distributioner). Du kan kompilera källkoden för
                MacOS X.
                </p>
                <br><br>
                <h3>Detta erbjuder streamCapture2</h3>
                <ol>
                    <li>Sök efter videoströmmar.</li>
                    <li>Ladda ner videoströmmar till din hårddisk.</li>
                    <li>Lägg till videoströmmar i en lista.</li>
                    <li>Ladda ner alla strömmar på listan samtidigt.</li>
                    <li>Ladda ner alla avsnitt i en tv-serie på en gång.</li>
                    <li>Välja kvalité på videofilen du laddar ner.</li>
                    <li>Välja med eller utan undertexter.</li>
                    <li>Automatisk kopiering av den nedladdae filen till valfri plats.</li>
                    <li>Kolla NFO-information om programmet.</li>
                    <li>Spara lösenord till lösenordsskyddade streamingtjänster.</li>
                    <li>Välj filnamn.</li>
                    <li>Skapa mappar, namngivna efter vald kvalité.</li>
                    <li>Alla versioner, Linux och Windows kan uppdatera sig själva, utom Windows "portable", där du måste ladda ner en ny version.</li>
                    <li>Stödjer "Drag och släpp", Dra adressen till en video till streamCapture2 och släpp.</li>
                    <li>Spara TV4 "Token" i programmet och ladda ner från TV4. </li>
                </ol>
                <br>
                <h3>Kolla <a href="https://bin.ceicer.com/streamcapture2/help-current/index_sv_SE.html" target="_blank">manualen</a>, där ser du hur det funkar.</h3>
                <br>
                
<!--
              
<h3>Version 2.20.9 BETA</h3>
            
<code>

Stil och tema.  <br>      
Version 2.20.6 BETA<br>
Välj mellan operativsystemets dialogrutor och programmets egna. "Verktyg" -> "Använd inte inbyggda dialogrutor".<br>
Förbättrat mörkt tema.<br>
Linux: Ljus eller mörkt tema, eller mörkt eller systemstandardtema, beroende på vilken Linux-distribution som används.<br>
Windows 10 och 11: Systemets standardtema eller mörkt tema.<br>
Windows 7 till 8.1: Ljus eller mörkt tema.<br><br>
<a href="https://bin.ceicer.com/streamcapture2/bin/windows/BETA/">Windows 10 och 11</a><br>
<a href="https://bin.ceicer.com/streamcapture2/bin/windows/obsolete/BETA/">Windows 7 och 8.1</a><br>
<a href="https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.27/BETA/">Linux GLIBC större än eller lika med 2.27</a><br>
<a href="https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.31/BETA/">Linux GLIBC större än eller lika med 2.31</a><br>
<a href="https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.35/BETA/">Linux GLIBC större än eller lika med 2.35</a><br>
<a href="https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.39/BETA/">Linux GLIBC större än eller lika med 2.39</a><br>

</code>
-->
<br>
<h3>Senaste version 3.0.0</h3>
<ol>
<li>streamCapture2 STÖDER INTE LÄNGRE Windows 7<br>eller tidigare versioner av Windows. Microsofts support för Windows 7<br>upphörde den 14 januari 2020.<br> Microsoft erbjuder inte längre säkerhetsuppdateringar.</li>
<li>streamCapture2 STÖDER INTE LÄNGRE<br>Linux med GLIBC-versioner lägre än 2.31.<br>GLIBC 2.31 släpptes 2020-02-01.</li>
<li>"download svtplay-dl" har gjorts om helt.<br>Programmet är responsivt och <br>fryser inte vid nedladdning.</li>
<li>"Skapa, importera och exportera nedladdningslistor.</li>
<li>FFmpeg uppdaterad till version 7.1</li>
<li>Windows: 7-Zip uppdaterad till version 24.09</li>
<li>Linux: CLI: Kontrollera FFmpeg-versionen.</li>
<li>Buggfixar och förbättringar.</li>
</ol>

<br>



        </section>


        <section class="mono">
        <h3> Testad framgångsrikt på</h3>
            <p>Arch Linux 20220510 (64-bit) AppImage<br>
            Bodhi Linux 6.0 (64-bit) AppImage<br>
            BionicPup32 8.0 (64-bit) AppImage<br>
            Fedora 32, Workstation Edition (64-bit) AppImage<br>
            Fedora 34, Workstation Edition (64-bit) AppImage<br>
            Fedora-Xfce-x86_64-40-1.14  (64-bit) AppImage Behöver installeras: "sudo dnf mesa-libGLU", "sudo dnf install pip", "pip install cryptography"<br>
            Kalix 2024.1 (64-bit) AppImage<br>
            KDE neon 20220505 (64-bit) AppImage<br>
            KDE neon 5.23.5 (64-bit) AppImage<br>
            Linuxmint 20 cinnamon (64-bit) AppImage<br>
            Lubuntu 19.10 (64-bit) AppImage<br>
            Lubuntu 20.10 (64-bit) AppImage<br>
            Lubuntu 21.10 (64-bit) AppImage<br>
            Lubuntu 22.04 (64-bit) AppImage<br>
            Manjaro-xfce 20.0.3-200606-linux56 (64-bit) AppImage<br>
            MX Linux 18.03 (64-bit) AppImage<br>
            MX Linux 23.01 (32-bit) AppImage<br>
            Peppermint 10 (32- and 64-bit) AppImage<br>
            Q4OS 3.11 (64-bit) AppImage<br>
            Salix 14.2.1 (64-bit) AppImage<br>
            Slackware 15.0 (64-bit) AppImage<br>
            Slackel 7.5 (32-bit) AppImage<br>
            Ubuntu 18.04 (32- and 64-bit) AppImage<br>
            Ubuntu 20.04 (64-bit) AppImage<br>
            Ubuntu 22.04 (64-bit) AppImage<br>
            Ubuntu 23.10 (64-bit) AppImage<br>
            Windows 7 (64-bit) Install, Portable<br>
            Windows 8.1 (64-bit) Install, Portable<br>
            Windows 10 (32- and 64-bit) Install, Portable<br>
            Windows 11 (64-bit) Install, Portable</p>


<br><br>
<h3>Om nedladdningen inte fungerar kan det bero på</h3>
<p>
<span class="fet">Att inställningarna för metod, bithastighet och avvikelse inte fungerar</span><br>
Om du låter programmet avgöra metod och bithastighet är chansen att lyckas mycket större än om du själv väljer olika inställningar för metod, bithastighet och avvikelse. När man väljer själv brukar man behöva prova olika inställningar innan det lyckas.
</p>
<br>
<p>
<span class="fet">Att Dolby Vision 4K videoströmmar saknas</span><br>
Använd endast "Dolby Vision 4K" om det finns någon sådan videoström. När du söker efter strömmande video och "dvhevc-51", "dvhevc", "hevc-51" eller "hevc" hittas av programet brukar det finnas Dolby Vision 4K videoströmmar. Hittas inga Dolby Vision 4K videoströmmar är risken stor att nedladdningen misslyckas.
</p>
        </section>

        <article>
            <h3>Språk</h3>
            <div class="flag">
            <figure>
                    <img src="images/british_smal.png" alt="British flag">
                    <figcaption>
                        Översatt till engelska</figcaption>
                </figure>
                <br>
                <figure class="topmargin10">
                    <img src="images/italy_smal.png" alt="Italien flag">
                    <figcaption>
                        Översatt till italienska</figcaption>
                </figure>
                <br>
                <figure class="topmargin10">
                    <img src="images/swedish_smal.png" alt="Swedish flag">
                    <figcaption>
                        Översatt till svenska</figcaption>
                </figure>
            </div> <!-- flag -->
            <br>
            <h3><a href="https://bin.ceicer.com/streamcapture2/help-v2.9/index.php?lang=sv_SE#form" target="_blank">Kontakta</a> mig! Rapportera en bugg eller fråga något.</h3>



<!--
<a href="https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.31/BETA/" target="_blank">Linux</a> | <a href="https://bin.ceicer.com/streamcapture2/bin/windows/BETA/" target="_blank">Windows</a>
-->
        </article>
 
    </div> <!-- content 1 -->



    <!-- content 2 -->
    <div class="content">
        <section>
        <h3>Testad med dessa sreamingleverantörer</h3>

    

<pre>
aftonbladet.se
dbtv.no
di.se
dn.se
dr.dk
efn.se
expressen.se
filmarkivet.se
flowonline.tv
nickelodeon.nl
nickelodeon.no
nickelodeon.se
nrk.no
oppetarkiv.se
pokemon.com
regeringen.se
ruv.is
riksdagen.se
svd.se
sverigesradio.se
svtplay.se
v3play.ee
tv3play.lt
tv3play.lv
tv4.se
tv4play.se
twitch.tv
ur.se
urplay.se
vasaloppet.se
vg.no
viagame.com
viafree.se/.dk/.no
viasat4play.no
viasatsport.se
</pre>
<br>
     <h3>DRM (Digital Rights Management) skyddat material går inte att ladda ner</h3>
    <p>Meddelande: "FEL: Inga videor hittades. Vi kan inte ladda ner DRM-skyddat innehåll från den här webbplatsen."</p>

<br><br>
    <h3>Ladda aldrig ner mina program från okända webbplatser</h3>
        <p>Du kan aldrig veta om du får originalfilen eller om de har lagt till något som kan skada din dator. SHA256-summan ska alltid matcha den du ser <a href="latest_sv.php">här</a>.</p><br>
        <p>Linux</p>
        <p class="mono"> sha256sum &lt;PROGRAM&gt;</p><br>
        
        <figure>
        <img class="dbclick" src="images/sha256sum.png" alt="Ubuntu">
                    <figcaption>
                        Beräkna SHA256 summa med Linux.
</figcaption>
                </figure>
          <br><br>    
        <p>Windows</p>
             <p class="mono"> certutil -hashfile &lt;PROGRAM&gt; sha256 </p><br>
        
        <figure>

        <img class="dbclick" src="images/certutil.png" alt="Windows">
                    <figcaption>
                        Beräkna SHA256 summa med Windows.
</figcaption>
                </figure>
                <br>
                Eller du kan ladda ner och använda mitt program <a href="https://gitlab.com/posktomten/hashsum/-/wikis/home" target="_blank">hashSum.</a>
               <br><br>
        </section>

        <section>
        <img src="images/linux_liten_fliped.png" alt="Linuxpingvin" class="virustotal">
               <h3>För Linuxanvändare (Ubuntu och Ubuntu-derivat) </h3>
               <p>Ett <a href="code.php" target="_blank">script </a> som kollar att Python och alla nödvändiga bibliotek för att
                exikvera svtplay-dl finns installerade. Saknas något så installerar scriptet det som fattas.
</p>
<br>
                <p>För att köra en Appimage i Ubuntu 22.04 och senare (och många fler nyare distributioner) kan du behöva installera fuse.</p>

<code>
sudo apt install libfuse2
</code>
<br><br>
Kontrollera att fuse (Filesystem in Userspace) är installerat på ditt system:
<br>
<code>
apt list --installed | grep libfuse*
</code>
<br>
<br>
<h3>För alla Linuxanvändare </h3>

<p>Ett CLI (Comand Line Interface) för Linux AppImage. Det ger tillgång till Linux/AppImage specifika inställningar. Testa "./streamcapture2-ffmpeg-x86_64.AppImage -h" (64-bit) eller "./streamcapture2-i386.AppImage -h" (32-bit). </p>

<br>
<code>
<a href="streamcapture2_help.php" target="_blank">./*.AppImage -h</a><br>
<a href="streamcapture2_help_all.php" target="_blank">./*.AppImage --help-all</a><br>
<a href="streamcapture2_appimage_help.php" target="_blank">./*.AppImage --appimage-help</a><br>

</code>
<br><br>






<p>Kontrollera din GLIBC-version:</p>
<code>ldd --version</code>
<br>
För att det ska fungera måste GLIBC versionen i ditt operativsystem vara samma eller högre än versionen som 
AppImagen använder.
<br><br>
<h3>Uppdatera streamCapture2 (<a href="https://youtu.be/hQm-Qg0MDbo?si=HEYPEtY2a4SzT-9P" target="_blank">Video</a>)</h3>
<br>
<figure>
<img class="dbclick" src="images/uppdatera.png" alt="Uppdatera streamCapture2">
<figcaption>
Du uppdaterar streamCapture2 genom att gå till "Verktyg", "Uppdatera". Det går bara att uppdatera om det finns en ny version.
Du får ett meddelande från programmet när det är dags att uppdatera.<br>Dubbelklicka för helskärm.
</figcaption>
                
</figure>




        
        </section>

        <article>
        <h3>Uppmuntran</h3>
        <p>Om du vill kan du uppmuntra och stötta mig med en liten summa, för en kopp kaffe eller så. Som öppenkällkodsutvecklare blir man glad när någon uppmärksammar vad man gör. Med Swish eller PayPal.</p>
       
        <figure class="topmargin40">
        <img id="swish" src="images/swish.png" alt="Swish">
                    <figcaption>
                       <h3>Swish 070 - 636 17 47<br>INGEMAR CEICER</h3></figcaption>
                </figure>

              
                <figure class="topmargin40">
        <img id="paypal" src="images/paypal.png" alt="PayPal">
                    <figcaption>
                       <a href="https://www.paypal.com/donate/?hosted_button_id=NA8UMFW9NJ4RG" target="_blank"><h3>PayPal</h3></a>

                       </figcaption>
                </figure>

         

       
        </article>
    </div> <!-- content 2 -->

    <footer>
        <div>&copy; Copyright 2016-<?php echo date("Y"); ?>
            Ingemar Ceicer<br>
            programming@ceicer.com
        
        </div>
    </footer>

</div> <!-- container -->

</body>

</html>
