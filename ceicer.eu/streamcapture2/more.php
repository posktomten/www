<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <script src="../jquery/jquery.js"></script>
    <title>streamCapture2</title>
</head>

<div class="container">
    <header>
    <img class="appicon" src="images/appicon.png" alt="streamCapture2 ikon">
        <p>streamCapture2</p>
   <!--     <img class="appicon" src="images/appicon.png" alt="streamCapture2 ikon"> -->
<script>
   $(document).ready(
      function(){
      $(".dbclick").dblclick(function(){
        this.requestFullscreen()
  }
)}

);

function copy(){
 
    var copyText = 'document.cookie.split("; ").find((row) => row.startsWith("tv4-refresh-token="))?.split("=")[1]; ';
  
   navigator.clipboard.writeText(copyText);
  
  // Alert the copied text
  alert("Skriptet har kopierats.\nAnvänd Ctrl + V för att klistra in.");

}
</script>
    </header>
    <nav>
    <div class="btn-group">
            <a class="button" href="../index.php">ceicer.eu</a>
            <a class="button" href="index.php">Sida 1</a>
            <a class="button" href="more.php">Sida 2</a>
            <a class="button" href="latest_sv.php">Ladda ner</a>
            <a class="button" href="http://bin.ceicer.com/streamcapture2/bin/" target="_blank">Ladda ner BETA</a>
            <a class="button" href=" https://bin.ceicer.com/streamcapture2/help-current/index_sv_SE.html" target="_blank">Manual</a>
            <a class="button" href="https://gitlab.com/posktomten/streamcapture2" target="_blank">Källkod</a>
            <a class="button" href="https://gitlab.com/posktomten/streamcapture2/-/wikis/home" target="_blank">Wiki (engelska)</a>
            <a class="button" href="https://gitlab.com/posktomten/streamcapture2/-/raw/master/LICENSE?ref_type=heads" target="_blank">Licens (engelska)</a>
            <a class="button" href="https://gitlab.com/posktomten/streamcapture2/-/raw/master/code/txt/readme_sv_SE.txt" target="_blank">Historik</a>


        </div> <!-- btn-group -->
    </nav>

    <div class="content">  <!-- content 1 -->

        <section>

            <h3>Svensk <a href="https://bin.ceicer.com/streamcapture2/help-current/index_sv_SE.html" target="_blank">manual</a>.</h3>


            <h3>Ladda ner från TV4</h3>
     
<ol>
<li>Information från <a href="https://svtplay-dl.se/tv4play/" target="_blank">svtplay-dl</a></li>
<li>Du måste vara inloggad på TV4-play.</li>
<li>Öppna "Tools", "Moore Tools", "Web developer tools"</li>
<li>Klicka på "Console"</li>
<li>En kommandoruta öppnas där det ska gå att köra program i webbläsaren.</li>
<li>Klistra in <span id="cpscript" onclick="copy()">skriptet</span> som letar upp din "Token". OBS! Man måste läsa noga vad webbläsaren meddelar. Ibland kan man behöva verifiera att det ska gå att köra skript. "Allow pasting" kan man behöva skriva in.</li>
<li>Om allt går bra så långt så kopierar man hela den långa raden med tecken.</li>
<li>Startar streamCapture2.</li>
<li>Öppna "TV4", "Ställ in TV4 Token".</li>
<li>Klistra in.
</ol>


<br>
      <!-- VIDEO -->

     
      <figure>
      <video controls>
      <source src="video/tv4token_sv.webm" type="video/webm">

     
     

      Tyvärr, din webbläsare stöder inte HTML5-media.
      </video>
      <figcaption>Instruktion för TV4 play.<br>Det ser ungefär likadant ut i Firefox, Chrome och i Edge. Det är dom som jag har testat.</figcaption>
        </figure>
      <br>
      <!--END  VIDEO -->

      
<h3>Fler <a href="https://gitlab.com/posktomten/streamcapture2/-/wikis/Manual" target="_blank"> videor</a></h3>
<h3><a href="https://gitlab.com/users/posktomten/projects" target="_blank">Fler</a> project som jag håller på med.</h3>

        </section>

        <section class="transparent">


<h3>TV4  (Dubbelklicka för helskärm)</h3>

<figure>
    <img class="dbclick" src="images/allow_pasting.png" alt="Tillåt att klistra in">
    <figcaption>
        Windows 10, Edge webbläsare.</figcaption>
</figure>

<br>
<h3>Bild på streamCapture2 (Dubbelklicka för helskärm)</h3>

<figure>
    <img class="dbclick" src="images/ladda_ner_ubuntu.png" alt="Ladda ner svtplay-dl, Ubuntu 22.04">
    <figcaption>
        Ladda ner svtplay-dl, Ubuntu 22.04</figcaption>
</figure>
<br><br>
<figure>
    <img class="dbclick" src="images/win11_sv.png" alt="Ladda ner svtplay-dl, Windows 11">
    <figcaption>
        Ladda ner svtplay-dl, Windows 11<a href="https://youtu.be/EQDEf0EhMMc?si=e_Hp9r0kMBC-WI5d"> video</a></figcaption>
</figure>

</section>

        <article>
<h3>Testa att mina programfiler är virusfria.</h3>
<p><a href="https://www.virustotal.com/gui/home/upload" target="_blank">VirusTotal</a><br>
Läs om VirusTotal: <a href="https://en.wikipedia.org/wiki/VirusTotal" target="_blank">Wikepedia</a><br>
Kan integreras med <a href="https://apps.microsoft.com/store/detail/virustotal-scan/9N4DV2XD731L?hl=en-en&gl=US" target="_blank">Windows</a><br>
Det är troligt att vissa antivirusprogram kommer att utfärda en varning.<br>
Inget som jag kan påverka.<br>
Detta händer vanligtvis när de inte känner till programmet.

<figure>
                    <img class="virustotal" src="images/virustotal.png" height="128" alt="VIRUSTOTAL">
                    <figcaption>
                        Virustotal</figcaption>
                </figure>



<hr>
<br><br>
<h3>Väldigt tacksamt om du meddelar mig om programmet uppför sig konstigt!</h3>
<p>Jag har ju ingen möjlighet att testa alla operativsystem eller alla skärmar med olika upplösning.</p>
<p>Eller så kanske du hittar buggar eller har frågor och synpunkter.</p>
<p>Mejla <a href="mailto:programming@ceicer.com">programming@ceicer.com</a></p>
<p><a href="https://bin.ceicer.com/streamcapture2/help-v2.9/index.php?lang=sv_SE#form" target="_blank">Eller använd detta formulär</a></p>
<figure>
                    <img src="images/computer.png" alt="Bild på dator">
                    <figcaption>
                        Ibland så krånglar det.</figcaption>
                </figure>
        </article>
       
    </div> <!-- content 1 -->


    <div class="content">  <!-- content 2 -->
        <section class="transparent">
        <h3>Bild på streamCapture2 (Dubbelklicka för helskärm)</h3>

        <figure>
                    <img class="dbclick" src="images/Windows10_dark.png" alt=" Windows 11, mörkt tema">
                    <figcaption>
                        Windows 11, mörkt tema</figcaption>
                </figure>
            
        </section>


        <section class="transparent">
        <h3>Bild på streamCapture2 (Dubbelklicka för helskärm)</h3>

        <figure>
                    <img class="dbclick" src="images/Ubuntu-22.04.png" alt="Windows 11, NFO information">
                    <figcaption>
                        Ubuntu 22.04</figcaption>
                </figure>

        </section>

        <article class="transparent">
        <h3>Bild på streamCapture2 (Dubbelklicka för helskärm)</h3>

<figure>
                    <img class="dbclick" src="images/streamcapture2_windows11_NFO.png" alt="Windows 11, NFO information">
                    <figcaption>
                    Windows 11, NFO information</figcaption>
                </figure>
        </article>
    </div> <!-- content 2 -->






    <footer>
        <div>&copy; Copyright 2016-<?php echo date("Y"); ?>
            Ingemar Ceicer<br>
            programming@ceicer.com
        </div>
    </footer>

</div> <!-- container -->

</body>

</html>