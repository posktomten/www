<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">
 <!--   <link rel="stylesheet" type="text/css" href="../../css/stilmall.css"> -->
  <!--  <link rel="stylesheet" type="text/css" href="../css/stilmall.css"> -->
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
   
    <script src="../../js/highlight.min.js"></script> 
     <script src="../../jquery/jquery.js"></script> 
    <script>
        hljs.highlightAll();

        function copyToClipboard() {
                            var range = document.createRange();
                            range.selectNode(document.getElementById("toclipboard"));
                            window.getSelection().removeAllRanges(); // clear current selection
                            window.getSelection().addRange(range); // to select text
                            document.execCommand("copy");
                            window.getSelection().removeAllRanges();// to deselect
                            alert("Innehållet i filen har kopierats till urklipp.");

                        }


    </script>
<script>
function atomonedark() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">');
}

function atomonelight() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-light.min.css">');
}
    </script>

<title>Installera alla Pythonbibliotek</title>
</head>
<body>
<section class="autowidth">
<h3>python_modules.sh &nbsp; | &nbsp; <span class="highlight" onclick="atomonelight()">Ljust</span>&nbsp; | &nbsp;<span class="highlight" onclick="atomonedark()">Mörkt</span> &nbsp; | &nbsp;<span class="highlight" onclick="copyToClipboard()">Kopiera till urklipp</span>  &nbsp;| &nbsp;<span class="highlight"><a href="../bin/script/python_modules.sh.tar.gz">Ladda ner</a></span> &nbsp; | &nbsp; <span class="highlight" onclick="window.close()">Stäng</span></h3>


<pre><code id="toclipboard">          
<?php
$file = file_get_contents("python_modules.sh");
$file_cleaned = htmlspecialchars($file, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5);
echo $file_cleaned;
?>
</code></pre>
</section>

</body>
</html>