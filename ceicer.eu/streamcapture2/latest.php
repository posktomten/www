<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <script src="../js/sorttable.js"></script>
    <title>streamCapture2</title>

<script>
    function comment(id){
        document.getElementById("comment").innerHTML="<p>"+comments[id]+"</p>" ;
  }

</script>



</head>

<div class="container">
<header>
    <img class="appicon" src="images/appicon.png" alt="streamCapture2 ikon">
        <p>streamCapture2</p>
       <!-- <img class="appicon" src="images/appicon.png" alt="streamCapture2 ikon"> -->

    </header>
    <nav>
        <div class="btn-group">
        <a class="button" href="http://bin.ceicer.com/streamcapture2/bin/" target="_blank">Download BETA</a>
            <a class="button" href="../index.php">ceicer.eu</a>
            <a class="button" href="latest_sv.php" target="_blank">På svenska</a>
            

        </div> <!-- btn-group -->
    </nav>

    <section id="comment" class="comment">
   
   <h2>Click on the rows in the table to get information about the versions.</h2>
   </section>   
          
<?php

require "../../streamcapture2";


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 

$rad=0;

try {

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT * FROM $tbstreamcapture2  ORDER BY _Operating_system, _Streamcapture2 ASC");
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
   
    echo "<div class=\"scroll-content\">";
    echo "<div class=\"table_background\"><table id=\"members\">";
    echo "<tr><th onclick=\"sortTable(0)\">Operating system</th><th onclick=\"sortTable(1)\">Bits</th><th onclick=\"sortTable(2)\">Python</th><th onclick=\"sortTable(3)\">Qt</th><th onclick=\"sortTable(4)\">Compiler</th><th onclick=\"sortTable(5)\">Compiler version</th><th onclick=\"sortTable(6)\">GLIBC</th><th onclick=\"sortTable(7)\">streamCapture2</th><th onclick=\"sortTable(8)\">File type</th><th onclick=\"sortTable(9)\">FFmpeg</th><th onclick=\"sortTable(10)\">Link</th><th onclick=\"sortTable(11)\">Link</th></tr>";
    $comment=array();
  
    $ffmpeginc="";
    foreach ($data as $row) {
        array_push($comment, $row['_comment']);
  

         echo "<tr id=\"".$rad."\" onclick=\"comment(this.id)\"><td>".$row['_Operating_system']."</td><td>".$row['_bit']."</td><td>".$row['_Python']."</td><td>".$row['_Qt']."</td><td>".$row['_Compiler']."</td><td>".$row['_Compiler_version']."</td><td>".$row['_GLIBC']."</td><td>".$row['_streamCapture2']."</td><td>".$row['_File_type']."</td><td>".$row['_FFmpeg_included']."</td><td><a href=\"".$row['_Download_Link']."\">Download</a></td><td><a href=\"".$row['_MD5_and_Size']."\" target=\"_blank\">SHA256 and Size</a></td></tr>";

         $rad++;
    }
    echo "</table></div>";
    echo "</div>";   
   
 

} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
  $conn = null;

?>
<script>
 var comments = <?php echo json_encode($comment); ?>
</script>




</div>
<footer>
        <div>&copy; Copyright 2016-<?php echo date("Y"); ?>
            Ingemar Ceicer<br>
            programming@ceicer.com
        </div>
    </footer>


</body>
</html>

