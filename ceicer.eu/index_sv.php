<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/dropdown.css">

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
   
    
    <title>ceicer.eu Startsida</title>
</head>
<body>
<div class="container">
    <header>
        <p>ceicer.eu</p>
    </header>

    <div class="content">
  
        <section>
            <h3> Välkommen</h3>

            <figure>
                <img src="images/ingemar.jpg" alt="The server">
                <figcaption>
                    Du är välkommen till ceicer.eu 
                </figcaption>
            </figure>
        </section>
        <section>

        <h3> Hårdvara</h3>

            <figure>
                <img src="images/server.jpg" alt="The server">
                <figcaption>
                    DELL LATITUDE E5470 
                </figcaption>
            </figure>

        </section>
        <section>

        <h3> Server och klient</h3>          
            <p>

            <?php echo "Server: ".$_SERVER['SERVER_NAME'];?><br>
            <?php echo "Administratör: ".$_SERVER['SERVER_ADMIN'];?><br>

            <?php echo "Klient: ".$_SERVER['REMOTE_ADDR'];?>
            
        </p>

        </section>

        <nav>
        <div class="btn-group">
            <div class="link_flag">
            <a class="button" href="index.php">ceicer.eu</a>            
            <img class="flag" src="images/english.png" alt="In English">
            </div>

            <div class="link_flag">
            <a class="button" href="index_sv.php">ceicer.eu</a>            
            <img class="flag" src="images/swedish.png" alt="På svenska">
            </div>

            <div class="link_flag">
            <a class="button" href="mail/index.php">Send e-mail</a>
            <img class="flag" src="images/english.png" alt="In English">
            </div>


            <div class="link_flag">
            <a class="button" href="bin/" target="_blank">ceicer.eu/bin</a>
            <img class="flag" src="images/english.png" alt="In English">
            </div>

            <div class="link_flag">
            <a class="button" href="https://bin.ceicer.com" target="_blank">bin.ceicer.com</a>
            <img class="flag" src="images/english.png" alt="In English">
            </div>

            <div class="link_flag">
            <a class="button" href="https://gitlab.com/users/posktomten/projects/" target="_blank">GitLab</a>
            <img class="flag" src="images/english.png" alt="In English">
            </div>
  
            <div class="link_flag">
            <a class="button" href="streamcapture2/index.php">streamCapture2</a>
            <img class="flag" src="images/swedish.png" alt="På svenska">
            </div>


           <!-- script -->
           <!-- script -->
           <div class="link_flag">
                <div class="dropdown">
                   <div class="button">Script</div>
                    <div class="dropdown-content">
                    <a class="button" href="script/build_appimage/script.php">Build AppImage</a>
                    <a class="button" href="script/batch_build_lib/script.php">Linux build</a>
                    <a class="button" href="script/batch_build_win_lib/script.php">Windows build</a>
                    <a class="button" href="script/unnecessary_files/script.php">Unnecessary files</a>
                    </div>
                </div>
                    <img class="flag" src="images/english.png" alt="In English">
            </div>

        </div> <!-- btn-group -->
    </nav>
    </div> <!-- content -->



<footer>
<div><p>&copy; Copyright 2023 - <?php echo date("Y"); ?>
        Ingemar Ceicer<br>
        programming@ceicer.com</p>
   
    </div>
</footer>

</div> <!-- container -->

</body>

</html>
