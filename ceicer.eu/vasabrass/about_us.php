<?php
session_start();
?>
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/about_us.css">
    <title>Vasa Brass - Hem</title>


</head>


<body>
    <div class="container">
        <header>
            <img class="logo" src="images/logo.jpg" alt="Logo">
            <div id="welcome">Välkommen till Vasa Brass</div>

        </header>
        <nav>
            <div class="btn-group">

                <a class="button" href="index.php">Hem</a>
                <a class="button" href="about_us.php">Om oss</a>
                <a class="button" href="members.php">Medlemmar</a>
                <a class="button" href="events.php">Kalender</a>
                <a class="button" href="https://soundcloud.com/user-274831219-500717936" target="_blank">Lyssna</a>
                <a class="button" href="photo_gallery.php">Fotogalleri</a>
                <a class="button" href="contact_us.php">Kontakta oss</a>
                <a class="button" href="https://www.facebook.com/vasabrass" target="_blank">Facebook</a>


            </div> <!-- btn-group -->
</nav>

<div class="content">

    <div class="left">
    <figure>
        <img class="fika" src="images/fika.jpg" alt="Musikerna fikar">
        <figcaption>
    Fikastund</figcaption>
    </figure>
     </div> <!-- left -->


    <div class="middle">
<p>Vasa Brass är ett brassband som bildades 1981 och har sin organisatoriska hemvist i Betlehemskyrkans Missionsförsamling i Göteborg.
Musikkåren består idag av drygt 25 musikanter och har en bred ekumenisk förankring bland sina medlemmar. Vasa Brass vill vara en samlingspunkt för musikanter som kanske inte musicerat under en lång följd av år, och får här en möjlighet att återuppta spelandet och samtidigt träffa både gamla och nya vänner.</p>
</div> <!-- middle -->
<div class="right"><p>
Därför är kaffepausen mitt i övningen ett viktigt inslag vid varje repetition liksom avslutningen. Musikkåren repeterar en gång i veckan i Bifrostkyrkan Mölndal och deltar med sin musik i ett antal gudstjänster och konserter varje termin.
Det sker framför allt i olika kyrkor i och runt Göteborg, men också på kortare turnéer.
Våren 2005 gjorde Vasa Brass en bejublad konsertturné till USA med Chicago som utgångspunkt. Läs mer om <a href="tour/midwest_tour_usa.php#start">Midwest Tour USA.</a></p>
    </div> <!-- right -->

    </div> <!-- content -->
  






    
  

    <footer>
            <div>Vasa Brass i sammarbete med<br>
                <img class="windcorp" src="images/windcorp.png" alt="windcorp"></div>
               
                <div>Webb av <a class="mailto" href="mailto:ingemar@ceicer.eu"> ingemar@ceicer.eu </a> Copyright © 2023 Vasa Brass. All Rights Reserved.</div>
 
</footer>

</div> <!-- container -->
</body>

</html>