<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

require "../../vasabrass";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$id=$_POST['id'];
//date("Y-m-d H:i:s");
//$date=$_POST['date'];
//$time=$_POST['time'];
//$place=$_POST['place'];
//$notes=$_POST['notes'];
//echo $time." ".$date;

//echo "id: ".$id." date:".$date." time:".$time." where:".$place." notes:".$notes;
//exit;

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Korrekt sätt att bygga SQL-frågan
      $sql = "DELETE FROM $tbevents WHERE _id=:id";
      $statement = $conn->prepare($sql);
      $statement->bindParam(':id', $id, PDO::PARAM_INT);
      if ($statement->execute()) {
      //  echo 'was deleted successfully.';
    }

 
} catch(PDOException $e) {
   // echo "Error: 2023-12([+-]?(?=\.\d|\d)(?:\d+)?(?:\.?\d*))(?:[Ee]([+-]?\d+))?" . $e->getMessage();
}
$conn = null;

header('Location: admin_events.php');
exit;

?>