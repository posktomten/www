<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

?>
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
    <link rel="stylesheet" type="text/css" href="css/admin_members.css">
    

    <title>Vasa Brass - Välj ny administratör</title>
>

</head>

<body>



<?php

require "../../vasabrass";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 

 

try {

     
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      /* ENDAST MEDLEMMAR SOM INTE ÄR ADMINISTRATÖRER LISTAS */
    $stmt = $conn->prepare("SELECT $tbmembers._id, _name, _musical_instrument FROM $tbmembers 
  
     LEFT JOIN $tbadministrators ON $tbadministrators._members_id = $tbmembers._id WHERE $tbadministrators._members_id IS NULL ORDER BY _name ASC"); 
  
   $stmt->execute();
   $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
   $rowcount = $stmt->rowCount();
   if ($rowcount==0){
    header("Location: no_member.php");
    exit;
   }
   
   
   echo "<div class=\"content\">";
   echo "<div class=\"attention stor\">Medlemmar</div>";
   // iterate over rows
  
   foreach($data as $row) { 

echo "<form action=\"new_administrator.php\" method=\"post\">";
echo "<div class=\"post\">";
echo "<div class=\"attention\">Namn</div>";
echo "<div class=\"post_detail\">".$row['_name']."</div>";
echo "<div class=\"attention\">Musikinstrument</div>";
echo "<div class=\"post_detail\">".$row['_musical_instrument']."</div>";
echo "<input type=\"hidden\" name=\"id\" value=\"".$row['_id']."\">";
echo "<input type=\"hidden\" name=\"name\" value=\"".$row['_name']."\">";
echo "<input type=\"submit\" class=\"run\" value=\"Välj\">";
echo "</div>"; // post
echo "</form>";

   }
echo "</div>"; // content
 
echo "<form action=\"admin_members.php\" method=\"post\">";
echo "<button type=\"submit\"  class=\"run nyknapp\">Avbryt</button>";
echo "</form>";


} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
  $conn = null;

  ?>

</body>
</html>

<!--
echo "<form action=\"new_administrator.php\" method=\"post\">";
echo "<td>".$row['_name'] ."</td><td>". $row['_musical_instrument']."</td><td class=\"knapp\"><button type=\"submit\"  class=\"run\">Välj</button></td>";
echo "<input type=\"hidden\" name=\"id\" value=\"".$row['_id']."\">";
echo "<input type=\"hidden\" name=\"name\" value=\"".$row['_name']."\">";
echo "</form>";
echo "</tr>";

   }
echo "</tbody>";
echo "</table>"; 
echo "</div>"; // list_users

echo "<div class=\"ny\">";
echo "<form action=\"admin_members.php\" method=\"post\">";
echo "<button type=\"submit\"  class=\"run nyknapp\">Avbryt</button>";
echo "</form>";
  -->