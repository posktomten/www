<?php
session_start();
?>
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="../css/gradient.css">
    <link rel="stylesheet" type="text/css" href="../css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="../css/about_us.css">

 


    <title>Vasa Brass - Hem</title>
</head>

<body>
    <!-- 1:a -->
    <div class="container">
        <header>
            <img class="logo" src="../images/logo.jpg" alt="Logo">
            <div id="welcome">Välkommen till Vasa Brass</div>

        </header>
        <nav>
            <div class="btn-group">

                <a class="button" href="../index.php">Hem</a>
                <a class="button" href="../about_us.php">Om oss</a>
                <a class="button" href="../members.php">Medlemmar</a>
                <a class="button" href="../events.php">Kalender</a>
                <a class="button" href="https://soundcloud.com/user-274831219-500717936" target="_blank">Lyssna</a>
                <a class="button" href="../photo_gallery.php">Fotogalleri</a>
                <a class="button" href="../contact_us.php">Kontakta oss</a>
                <a class="button" href="https://www.facebook.com/vasabrass" target="_blank">Facebook</a>


            </div> <!-- btn-group -->
</nav>

        <!-- 1:a -->
        <a id="start"></a>
<div class="content">

    <div class="left">
    <figure>
        <img class="turnee8" src="images/turnee8.jpg" alt="Bild från flygplanet till Chicago">
        <figcaption>Bild: Gunnar Stenström</figcaption>
    </figure>
     </div> <!-- left -->


    <div class="right">
<p>Den 29 april flög 23 musikanter och 13 medresenärer från Landvetter till Chicago via Köpenhamn. Vi mottogs på O´Hare flygplatsen av våra reseledare Åke Hedström och LeRoy Nelson. Därefter tillbringade vi tre dagar i Chicago med konserter i Winettka Covenant Churh och North Park Covenat Church där vi även medverkade i gudstjänsten på söndagen. Vi hann även med att göra en sightseeingtur i centrala staden och ett besök uppe i Hancock, Chicagos näst högsta byggnad, med vidunderlig utsikt över staden och dess omgivningar.</p>
</div> <!-- right -->
<div class="middle"><p>
Turen gick sedan med buss mot nordväst till Rockford, Madison och Mason City med konsert på varje plats. I Madison och Mason City övernattade vi i olika hem. Resan fortsatte mot svenskbygderna i Minnesota där vi fick en mycket intressant dag, som vi sällan glömmer, där vi besökte platser dit svenskarna en gång utvandrade såsom Chisaga, Lindström, Skandia samt Vilhelm Mobergs fiktiva invandrarstuga ”Duvemåla”.</p>
    </div> <!-- middle -->

    </div> <!-- content -->

    <!-- 2:a -->
 
    <div class="content">

<div class="left">
<figure>
    <img class="turnee11" src="images/turnee11.jpg" alt="Bild från svenskbygderna.">
    <figcaption>Bild: Gunnar Stenström</figcaption>
</figure>
 </div> <!-- left -->


<div class="right">
<p>Vi gjorde också ett besök i Mall of America, ett av USA:s största varuhus, som inte låg så långt ifrån Minneapolis där vi stannade i tre dagar. Vi höll tre konserter i Minneapolis, en i Swedish America Institute, en i Bloomington Convenant Church och slutligen en i Salem Convenant Church. Under besöket i Minneapolis fick vi en sightseeing både i Swedish America Institute och i The Capitol i S:t Paul, tvillingstad med Minneapolis. På tillbakavägen till Chicago besökte vi bl.a. platser med de svenska namnen Stockholm och Lund. Den 10 maj tog vi adjö av våra reseledare och äntrade planet som tog oss hem till Göteborg via Stockholm.</p>
</div> <!-- right -->
<div class="middle"><p>
USA-resan var en otrolig fin upplevelse både för musikanter och medresenärer. Vi fick se och uppleva väldigt mycket tack vare våra eminenta reseledare. Våra konserter var mycket uppskattade och välbesökta.<br><br>Lars Sewerinson</p>
</div> <!-- middle -->

</div> <!-- content -->


<footer>
            <div>Vasa Brass i sammarbete med<br>
                <img class="windcorp" src="../images/windcorp.png" alt="windcorp"></div>
               
                <div>Webb av <a class="mailto" href="mailto:ingemar@ceicer.eu"> ingemar@ceicer.eu </a>  Copyright © 2023 Vasa Brass. All Rights Reserved.</div>
 
</footer>
    
    </div> <!-- container -->


</body>

</html>