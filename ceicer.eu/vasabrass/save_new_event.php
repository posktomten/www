<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

require "../../vasabrass";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//$id=$_POST['id'];

$date=$_POST['date'];
$day_of_week=$_POST['day_of_week'];
$time=$_POST['time'];
$place=$_POST['place'];
$notes=$_POST['notes'];

//echo $time." ".$date;

//echo "id: ".$id." date:".$date." time:".$time." where:".$place." notes:".$notes;
//exit;

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Korrekt sätt att bygga SQL-frågan
      $sql = "INSERT INTO $tbevents ( _date, _day_of_week, _time, _place, _notes) VALUES ('$date','$day_of_week', '$time','$place','$notes')";
    $stmt = $conn->prepare($sql);
    $conn->exec($sql);
    //echo "Record updated successfully";
 
} catch(PDOException $e) {
    //echo "Error: " . $e->getMessage();
}
$conn = null;

header('Location: admin_events.php');
exit;

?>