<?php
session_start();
?>
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
 
    
 
   <title>Vasa Brass - Hem</title>
   


</head>

<body>
    <div class="container">
        <header>
            <img class="logo" src="images/logo.jpg" alt="Logo">
            <div id="welcome">Välkommen till Vasa Brass</div>

        </header>
        <nav>
            <div class="btn-group">

                <a class="button" href="index.php">Hem</a>
                <a class="button" href="about_us.php">Om oss</a>
                <a class="button" href="members.php">Medlemmar</a>
                <a class="button" href="events.php">Kalender</a>
                <a class="button" href="https://soundcloud.com/user-274831219-500717936" target="_blank">Lyssna</a>
                <a class="button" href="photo_gallery.php">Fotogalleri</a>
                <a class="button" href="contact_us.php">Kontakta oss</a>
                <a class="button" href="https://www.facebook.com/vasabrass" target="_blank">Facebook</a>


            </div> <!-- btn-group -->
        </nav>

        <img class="frontimage" src="images/frontimage.jpg" alt="Front image">


            <footer class="footer_loggin">

            <div>Vasa Brass i sammarbete med<br>
                <img class="windcorp" src="images/windcorp.png" alt="Windcorp"></div>
               
            <div>Webb av <a class="mailto" href=ingemar@ceicer.eu> ingemar@ceicer.eu </a>  Copyright © 2023 Vasa Brass. All Rights Reserved.</div>
            
<?php
           if(isset($_SESSION['logged_in'])){
               
                echo "<form action=\"logged_out.php\" method=\"post\">"; 
                echo "<input type=\"submit\" value=\"Logga ut\">";
                echo "</form>";
            } else {

                echo "<form action=\"loggin.php\" method=\"post\">"; 
                echo "<input type=\"submit\" value=\"Logga in\">";
                echo "</form>";
            }
?>

            
            
</footer>
    
    </div> <!-- container -->


</body>

</html>