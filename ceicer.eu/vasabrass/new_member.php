<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

$id = $_POST['id'];
?>

<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/detail.css">
    

    <title>Vasa Brass - Ny medlem</title>
    

</head>
<body>
<?php

  
      
  
        echo "<div id=\"detail\">";
        echo "<form action=\"save_new_member.php\" method=\"post\">";  

        echo "<label for=\"name\">Namn</label>"; 
        echo "<input  autocomplete=\"on\" name=\"name\" id=\"name\" type=\"text\">";
        
        echo "<label  for=\"musical_instrument\">Musikinstrument</label>";
        echo "<input  autocomplete=\"on\" name=\"musical_instrument\" id=\"musical_instrument\" type=\"text\">";

        echo "<input  type=\"submit\" value=\"Spara\">";
        echo "<input name=\"cancel\" type=\"submit\" value=\"Avbryt\">";
        echo "</form>";
        echo "</div>"; 
    
?>

</body>
</html>
