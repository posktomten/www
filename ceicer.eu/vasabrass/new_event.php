<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

?>

<!DOCTYPE html>
<!--
ALLA EVENT LISTAS
MÖJLIGHET ATT REDIGERA OCH ATT TA BORT OCH LÄGGA TILL
-->
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/admin.css"> 
    <link rel="stylesheet" type="text/css" href="css/detail_events.css">
    

    <title>Vasa Brass - Ny kalenderpost</title>


</head>

<body>


 
<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 
echo "<div class=\"content\">";


echo "<div class=\"event_members\">";


echo "<form action=\"save_new_event.php\" method=\"post\">";
    echo "<div>";
        echo "<label for=\"date\">Datum</label>";
        echo "<input type=\"text\" id=\"date\" name=\"date\" placeholder=\"YYYY-MM-DD\">";
    echo "</div>";  /* date */

    echo "<div>";
    echo "<label for=\"day_of_week\">Veckodag</label>";
    echo "<input type=\"text\" id=\"day_of_week\" name=\"day_of_week\">";
echo "</div>";  /* day_of_week */

    echo "<div>";
        echo "<label for=\"time\">Klockslag</label>";
        echo "<input type=\"text\" name=\"time\" id=\"time\">";
    echo "</div>";  /* time */

    echo "<div>"; 
        echo "<label for=\"place\">Plats</label>";
        echo "<input type=\"text\" name=\"place\" id=\"place\">";
    echo "</div>";  /* place */

    echo "<div>";  
        echo "<label for=\"notes\">Noteringar</label>";
        echo "<textarea name=\"notes\" id=\"notes\" placeholder=\"Notera att du måste skriva datum på formen ÅÅÅÅ-MM-DD, till exempel 2023-08-23\"></textarea>";
        echo "<input type=\"hidden\" name=\"id\">";
    echo "</div>";  /* notice */






  echo "<div class=\"ny\">";
//  echo "<form action=\"save_events_details.php\" method=\"post\">";
  echo "<button type=\"submit\" class=\"run nyknapp\">Spara</button>";
  echo "</form>";


  echo "<form action=\"admin_events.php\" method=\"post\">";
  echo "<button type=\"submit\" class=\"run nyknapp\">Avbryt</button>";
  echo "</form>";

  echo "<form action=\"index.php\" method=\"post\">";
  echo "<button type=\"submit\" class=\"run nyknapp\">Startsidan</button>";
  echo "</form>";
  
  echo "</div>"; /* ny */
  
  echo "</div>"; // event_members
  echo "</div>"; // content

?>
</body>
</html>