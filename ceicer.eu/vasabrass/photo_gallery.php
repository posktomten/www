<?php
session_start();
?>
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/photo_gallery.css">
    <script src="../jquery/jquery.js"></script>
    
    <script>
        // When webpage will load, everytime below 
        // function will be executed
        $(document).ready(function () {
  
            // If user clicks on any thumbanil,
            // we will get it's image URL
            $('.thumbnails img').on({
                click: function () {
                    let thumbnailURL = $(this).attr('src');
  
                    // Replace main image's src attribute value 
                    // by clicked thumbanail's src attribute value
                    $('figure img').fadeOut(200, function () {
                        $(this).attr('src', thumbnailURL);
                    }).fadeIn(200);
                }
            });
        });


        $(document).ready(

            function(){
                $("img").dblclick(function(){
                    this.requestFullscreen()
              }
            )}
            
        );



    </script>

   

    <title>Vasa Brass - Fotogalleri</title>
</head>

<body>

  
        <header>
            <img class="logo" src="images/logo.jpg" alt="Logo">
            <div id="welcome">Välkommen till Vasa Brass</div>

        </header>
        <nav>
            <div class="btn-group">

                <a class="button" href="index.php">Hem</a>
                <a class="button" href="about_us.php">Om oss</a>
                <a class="button" href="members.php">Medlemmar</a>
                <a class="button" href="events.php">Kalender</a>
                <a class="button" href="https://soundcloud.com/user-274831219-500717936" target="_blank">Lyssna</a>
                <a class="button" href="photo_gallery.php">Fotogalleri</a>
                <a class="button" href="contact_us.php">Kontakta oss</a>
                <a class="button" href="https://www.facebook.com/vasabrass" target="_blank">Facebook</a>


            </div> <!-- btn-group -->
        </nav>   
        <div class="content">   
<div class="mainDiv">
  <div class="all_thumbnails">
  <!--div for left thumbanails-->
  <div class="thumbnails">
      <img src="images/photo_gallery/no01.jpg" alt="bild 1">
      <img src="images/photo_gallery/no02.jpg" alt="bild 2">
      <img src="images/photo_gallery/no03.jpg" alt="bild 3">
      <img src="images/photo_gallery/no04.jpg" alt="bild 4">
      <img src="images/photo_gallery/no05.jpg" alt="bild 5">
      <img src="images/photo_gallery/no06.jpg" alt="bild 6">
      <img src="images/photo_gallery/no07.jpg" alt="bild 7">
  </div>



  <!--div for right thumbanails-->
  <div class="thumbnails">

     
      <img src="images/photo_gallery/no08.jpg" alt="bild 8">
      <img src="images/photo_gallery/no09.jpg" alt="bild 9">
      <img src="images/photo_gallery/no10.jpg" alt="bild 10">
      <img src="images/photo_gallery/no11.jpg" alt="bild 11">
      <img src="images/photo_gallery/no12.jpg" alt="bild 12">
      <img src="images/photo_gallery/no13.jpg" alt="bild 13">
  </div>
</div> <!-- all_thumbnails -->
    <!--div for main image-->
   
    <figure>
      <img src="images/photo_gallery/no01.jpg" alt="bild 1">
      <figcaption class="attention">
      Dubbelklicka för helskärmsbild</figcaption>
    </figure>
   
</div>



</div> <!-- content -->


<footer class="gallery">
            <div>Vasa Brass i sammarbete med<br>
                <img class="windcorp" src="images/windcorp.png" alt="windcorp"></div>
               
            <div>Webb av <a class="mailto" href="mailto:ingemar@ceicer.eu"> ingemar@ceicer.eu </a>  Copyright © 2023 Vasa Brass. All Rights Reserved.</div>
 
</footer>




</body>

</html>

