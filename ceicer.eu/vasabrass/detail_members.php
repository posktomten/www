<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

$id = $_POST['id'];

?>

<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/detail.css">
    
    <title>Vasa Brass - Editera medlem</title>

</head>
<body>
<?php

    require "../../vasabrass";

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

  try {
   $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $stmt = $conn->prepare("SELECT _id, _name, _musical_instrument FROM $tbmembers WHERE _id = '$id' LIMIT 1");

       $stmt->execute();
    
       $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

      foreach ($result as $row) {
      
  
          echo "<div id=\"detail\">";
          echo "<form action=\"edit_members.php\" method=\"post\">";  
          echo "<label for=\"name\">Namn</label>"; 
      
            echo "<input  autocomplete=\"on\" name=\"name\" id=\"name\" type=\"text\" value=\"".$row['_name']."\">";
            echo "<label  for=\"musical_instrument\">Musikinstrument</label>";
            echo "<input  autocomplete=\"on\" name=\"musical_instrument\" id=\"musical_instrument\" type=\"text\" value=\"" . $row['_musical_instrument'] . "\">";

            echo "<label class=\"container\">Ta bort";
            echo "<input  autocomplete=\"on\" name=\"del\" type=\"checkbox\" value=\"Ta bort\"><br>";
            echo "<span class=\"checkmark\"></span>";
            echo "</label>";
            echo "<input type=\"hidden\" name=\"id\" value=\"".$id."\">";
            echo "<input  type=\"submit\" value=\"Spara\">";
            echo "<input name=\"cancel\" type=\"submit\" value=\"Avbryt\">";
            echo "</form>";
            echo "</div>"; 
        }

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    $conn = null;
?>

</body>
</html>
