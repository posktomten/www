<?php
session_start();
?>
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/about_us.css">
    
 <title>Vasa Brass - Kontakta oss</title>

</head>

<body>
    <div class="container">
        <header>
            <img class="logo" src="images/logo.jpg" alt="Logo">
            <div id="welcome">Välkommen till Vasa Brass</div>

        </header>
        <nav>
            <div class="btn-group">

                <a class="button" href="index.php">Hem</a>
                <a class="button" href="about_us.php">Om oss</a>
                <a class="button" href="members.php">Medlemmar</a>
                <a class="button" href="events.php">Kalender</a>
                <a class="button" href="https://soundcloud.com/user-274831219-500717936" target="_blank">Lyssna</a>
                <a class="button" href="photo_gallery.php">Fotogalleri</a>
                <a class="button" href="contact_us.php">Kontakta oss</a>
                <a class="button" href="https://www.facebook.com/vasabrass" target="_blank">Facebook</a>


            </div> <!-- btn-group -->
</nav>

        <div class="content">

<div class="left">
<figure>
    <img class="fika" src="images/bifrostkyrkan.jpg" alt="Musikerna fikar">
    <figcaption>
Bifrostkyrkan</figcaption>
</figure>
 </div> <!-- left -->


<div class="middle">

   <p> Bokningar görs genom<br>
 
   Ordförande: Per-Ola Park</p>
 
 <p>     
 Frågor och information<br>
 Har du frågor eller vill veta mer om Vasa Brass? Då är du välkommen att kontakta oss via e-post:<br>
 <a href="mailto:info@vasabrass.se"> info@vasabrass.se </a>  </p>
 
 <p>Vi repeterar i Bifrostkyrkan</p>


</div> <!-- middle -->
<div class="right"><p>
Hitta till Bifrostkyrkan:</p>
<a href="https://goo.gl/maps/fqr3QXQBHaqmhnb8A" target="_blank">Google Maps</a>
</div> <!-- right -->

</div> <!-- content -->




<footer>
            <div>Vasa Brass i sammarbete med<br>
                <img class="windcorp" src="images/windcorp.png" alt="Windcorp"></div>
               
                <div>Webb av <a class="mailto" href="mailto:ingemar@ceicer.eu"> ingemar@ceicer.eu </a>  Copyright © 2023 Vasa Brass. All Rights Reserved.</div>

</footer>
    
    </div> <!-- container -->


</body>

</html>
