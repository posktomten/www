<?php
session_start();
?>
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/detail.css">
    <link rel="stylesheet" type="text/css" href="css/about_us.css">
    


    <title>Vasa Brass - Logga in</title>

</head>
<body>

<?php

$usrname=$_POST['usrname'];
$passwd=$_POST['passwd'];



require "../../vasabrass";


// KOLLAR OM ANVÄNDARNAMNET FINNS

try{  
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $statement = $conn->prepare("SELECT _usrname from $tbadministrators WHERE 
    _usrname=:usrname LIMIT 1");

$statement->execute(array(':usrname'=>$usrname));

$output=$statement->fetchAll(PDO::FETCH_ASSOC); 

if (!$output){
  echo "<div id=\"detail\">";
  echo "<form action=\"index.php\" method=\"post\">";
  echo "<label for=\"sub\">".$usrname." loggades inte in. Kontrollera användarnamn och lösenord.</label>";
  echo "<input type=\"submit\" id=\"sub\" name=\"submit\" value=\"Ok\">";
  echo "</form>";
  echo "</div>";
  exit;
} 

} catch (PDOException $e) {
echo "Fel 1: " . $e->getMessage();
}

$conn = null;

//   Yu65%!(jjj6y

$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
 /* Look for the username in the database. */
$query = "SELECT * FROM $tbadministrators WHERE (_usrname = :usrname)";
/* Values array for PDO. */
$values = [':usrname' => $usrname];
/* Execute the query */
try
{
  $res = $conn->prepare($query);
  $res->execute($values);
  
}
catch (PDOException $e)
{
  /* Query error. */
  echo 'Query error.'.$e->getMessage();
  die();
}
$row = $res->fetch(PDO::FETCH_ASSOC);
$hached_passwd=$row['_passwd'];
//$hashed_passwd=$row['_passwd'];
//echo $hashed_passwd."<br>";
//echo $usrname."<br>";
//echo $passwd."<br>";
//echo $hached_passwd."<br>";
/* If there is a result, check if the password matches using password_verify(). */


if (is_array($row))
{
  if (password_verify($passwd, $hached_passwd))
  { // INLOGGAD
    $_SESSION['logged_in']=$row['_members_id'];
    echo "<div id=\"detail\">";
    echo "<form action=\"index.php\" method=\"post\">";
    echo "<label for=\"submit\">Välkommen ".$usrname."!<br>Du är inloggad. Enligt EU:s lagstiftning angående \"GDPR\" och \"the ePrivacy Directive\" informeras du nu om att denna webbsida använder \"Session Cockies\". Dessa cockies används för att att säkerställa att du, när du är inloggad, kommer åt lösenordsskyddad data. Session Cockies raderas när du loggar ut eller stänger din webbläsare.<br>  </label>";
    echo "<input type=\"submit\" id=\"submit\" name=\"submit\" value=\"Ok\">";
    echo "</form>";
    echo "</div>";
   ?>
    <div class="content">

    <div class="left">
    <figure>
        <img class="fika" src="images/security.webp" alt="Security">
        <figcaption>
    En god säkerhet.</figcaption>
    </figure>
     </div> <!-- left -->
    
    
    <div class="middle">
    
       <h4> Sex regler om lösenord</h5>
       <ol>
      <li> Lösenordet måste innehålla minst 8 tecken.</li>
      <li> Minst 1 tecken måste vara en liten bokstav ("abc").</li>
      <li> Minst 1 tecken måste vara en STOR bokstav ("ABC").</li>
      <li> Minst 1 tecken måste vara en siffra ("123").</li>
      <li> Minst 1 tecken måste vara ett specialtecken ("&!,").</li>
      <li> Lösenordet får inte innehålla några blanktecken ("Mellanslag).</li>
     </ol>
     <h4> Två regler om användarnamn</h4>
       <ol>
      <li> Användarnamnet måste vara unikt.</li>
      <li> Användarnamnet får inte innehålla några blanktecken ("Mellanslag").</li>
     </ol>

     <h4> En regel om datum för kalenderhändelser</h4>
       <ol>
      <li> Du måste skriva datum på formen ÅÅÅÅ-MM-DD (2023-06-27).</li>
     </ol>
    
    
    
    </div> <!-- middle -->
    <div class="right">
  <p>Ditt lösenord skickas krypterat till servern. Servern har sparat en kontrollsumma som matchas 
    mot ditt lösenord. Ditt lösenord sparas inte. Det innebär att du ALDRIG
    kan få reda på ditt lösenord om du glömt av det.</p>
    <p>Det måste alltid finnas minst en administratör. Den sista administratören kan inte tas bort.</p> Servern kollar automatiskt att du valt ett unikt och korrekt användarnamn och ett korrekt lösenord.<p>Det är endast medlemmar som kan utses till administratörer.</p><p>Anledningen till att datum för en post måste skrivas exakt ÅÅÅÅ-MM-DD är att servern sorterar posterna i tidsordning. Den senaste kalenderhändelsen hamnar överst.</p>



    </div> <!-- right -->
    
    </div> <!-- content -->
<?php

    exit;
  }
  else {
    echo "<div id=\"detail\">";
    echo "<form action=\"index.php\" method=\"post\">";
    echo "<label for=\"sub\">".$usrname." loggades inte in. Kontrollera användarnamn och lösenord.</label>";
    echo "<input type=\"submit\" id=\"sub\" name=\"submit\" value=\"Ok\">";
    echo "</form>";
    echo "</div>";
    exit;
  }
}

$conn = null;

?>

</body>
</html>
