<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

if (isset($_POST['new_admin'])){
header("Location: new_admin.php");
}

if (isset($_POST['new_member'])){
    header("Location: new_member.php");
}



$id = $_POST['id'];
$name = $_POST['name'];
?>

<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/detail.css">
    
    <title>Vasa Brass - Detalj administratör</title>

</head>
<body>
<?php

    require "../../vasabrass";

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    try {

        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $conn->prepare("SELECT $tbadministrators._id AS adm_id, $tbadministrators._members_id, $tbadministrators._usrname, $tbadministrators._passwd, $tbmembers._name AS name FROM $tbadministrators, $tbmembers WHERE  $tbadministrators._members_id = $tbmembers._id AND $tbmembers._id='$id' LIMIT 1");  

        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
     
     



   
          
      foreach ($data as $row) {
      
    
          echo "<div id=\"detail\">";
          
          echo "<form action=\"edit_administrators.php\" method=\"post\">";  
          echo "<div class=\"attention\">Användarnamn och lösenord för ".$name."</div>";
          echo "<label for=\"usrname\">Användarnamn</label>"; 
          echo "<input id=\"usrname\" name=\"usrname\" type=\"text\" value=\"".$row['_usrname']."\">";

          echo "<label for=\"passwd\">Lösenord</label>"; 
          echo "<input id=\"passwd\" name=\"passwd\" type=\"text\">";
/*
            echo "<label class=\"container\">Administratör";
                echo "<input name=\"is_admin\" type=\"checkbox\" value=\"Administrator\" checked=\"checked\">";

            echo "<span class=\"checkmark\"></span>";
            echo "</label>";
*/
            echo "<input type=\"hidden\" name=\"id\" value=\"".$row['adm_id']."\">";
            echo "<input type=\"hidden\" name=\"name\" value=\"".$row['name']."\">";
            echo "<input  type=\"submit\" value=\"Spara\">";
            echo "<input name=\"cancel\" type=\"submit\" value=\"Avbryt\">";
            echo "</form>";
            echo "<form action=\"delete_administrator.php\" method=\"post\">";
            echo "<input  type=\"submit\" value=\"Ej administratör\">";
            echo "<input type=\"hidden\" name=\"name\" value=\"".$row['name']."\">";
            
            echo "<input type=\"hidden\" name=\"adm_id\" value=\"".$row['adm_id']."\">";
            echo "</form>";
            echo "</div>"; 
        }

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    $conn = null;

 
?>

</body>
</html>
