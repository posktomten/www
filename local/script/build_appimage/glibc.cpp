#include <stdio.h>
#ifdef __GLIBC__
#include <gnu/libc-version.h>
#endif

int main(void)
{
    #ifdef __GLIBC__
        printf("%u.%u\n", __GLIBC__, __GLIBC_MINOR__);
        return 0;
    #else
        return 1;
    #endif
}
