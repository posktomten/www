<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="../css/stilmall.css">
   
    <script src="../../js/highlight.min.js"></script>
    <script src="../../jquery/jquery.js"></script>
    <script>
        hljs.highlightAll();
    </script>

    <script>
        $(document).ready(
            function () {
                $("img").dblclick(function () {
                    this.requestFullscreen()
                })
            }

        );

            function copyToClipboard(toclipboard) {
                            var range = document.createRange();
                            range.selectNode(document.getElementById(toclipboard));
                            window.getSelection().removeAllRanges(); // clear current selection
                            window.getSelection().addRange(range); // to select text
                            document.execCommand("copy");
                            window.getSelection().removeAllRanges();// to deselect
                            alert("The contents of the file have been copied to the clipboard.");
                        }

function atomonedark() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">');
}

function atomonelight() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-light.min.css">');
}

    </script>

    <title>Build AppImage</title>
</head>

<div class="container">
    <header>
        <p>BASH script build AppImage</p>


    </header>
    <nav>
        <div class="btn-group">
            <a class="button" href="../../index.php">ceicer.eu</a>



        </div> <!-- btn-group -->
    </nav>
    <div class="content">
        <!-- content 1 -->
        <section class="smal">
            <h3> A script to automate the creation of AppImages and to upload the finished AppImage to a server</h3>
            <h3>This AppImage can <a href="https://youtu.be/RTOopvPoOMI" target="_blank">update itself</a>. I use zsync to update. Which means that very little data has to be downloaded when updating.</h3>

            <h3>The script detects whether it is 32- or 64-bit architecture.</h3>
            
            <p>You need to edit to match the versions of Qt you are using. You also need to adapt the script to the folder structure of the server to which you upload the finished AppImage.
            </p>
            <br>
            <p>The script also creates a <a href="qwordcount-x86_64.AppImage-MD5.txt" target="_blank">text file </a>with information about the AppImage's MD5 sum, the ApppImage's size and the date and time of the upload. The text file is uploaded at the same time as the AppImage is uploaded to the server.
            </p>
           
            
            <div class="row">
            <span class="highlight" onclick="atomonelight()">Light</span>&nbsp; | &nbsp;<span class="highlight" onclick="atomonedark()">Dark</span>
            </div>
<pre><code>
# EDIT
QT5=5.15.10
QT6=6.5.2
EXECUTABLE=streamcapture2
SUPORTEDGLIBC=2.31
# STOP EDIT
BIN_DIR="/bin.ceicer.com/public_html/${EXECUTABLE}/bin/linux/GLIBC${GLIBCVERSION}"
ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}/"
ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}"
</code></pre>
<br>
<h3>My script uses</h3>

<p><a href="https://www.qt.io/download-open-source" target="_blank">Qt</a></p>
<p><a href="https://github.com/probonopd/linuxdeployqt" target="_blank">linuxdeployqt</a></p>
<p><a href="https://github.com/AppImage/appimagetool" target="_blank">appimagetool</a></p>
<p><a href="http://zsync.moria.org.uk/" target="_blank">zsync</a></p>
<br>
            <br>
            <h3>Example (Double click for full screen)</h3>
            <figure>
                <img src="images/image1.png" alt="Step 1">
                <figcaption>
                    Step 1 
                </figcaption>
            </figure>
            <br>
            <figure>
                <img src="images/image2.png" alt="Step 2">
                <figcaption>
                    Step 2 
                </figcaption>
            </figure>
            <br>
            <figure>
                <img src="images/image3.png" alt="Step 3">
                <figcaption>
                    Step 3
                </figcaption>
            </figure>
            <br>
            <figure>
                <img src="images/image4.png" alt="Step 4">
                <figcaption>
                    Step 4
                </figcaption>
            </figure>
            <br>
            <figure>
                <img src="images/image5.png" alt="Step 5">
                <figcaption>
                    Step 5
                </figcaption>
            </figure>
            <br>
            <figure>
                <img src="images/image6.png" alt="Step 6">
                <figcaption>
                    Step 6
                </figcaption>
            </figure>
           <br>

                    <h3>Download script</h3> 
                    <p><a href="../../bin/script/build_appimage.sh.tar.gz">build_appimage.sh.tar.gz</a></p>

<!-- EXTRA VÄNSTER -->
                    <div>
        <section class="smal">

        <h3>I use a small <a href="../../bin/script/glibc.tar.gz" target="_blank">c++ program </a> so that the script knows which GLIBC version is used when creating the AppImage. A script works just as well.</h3>
    
        </section>

        <section class="smal" id="glibc">
       <h3>The source code for the c++ program &nbsp; | &nbsp; <span class="highlight" onclick="atomonelight()">Light</span>&nbsp; | &nbsp;<span class="highlight" onclick="atomonedark()">Dark</span>&nbsp; | &nbsp; <span class="highlight" onclick="copyToClipboard('cpp')">Copy to clipboard</span></h3>
<pre><code id="cpp">
<?php
$file = file_get_contents("glibc.cpp");
$file_cleaned = htmlspecialchars($file, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5);
echo $file_cleaned;
?>
</code></pre>
        
       </section>

<!--
       <article>
       content 2, article

      </article>
-->

    </div> 


<!-- HÄR -->
        </section>


        <section class="big">
        <h3>build_appimage.sh &nbsp; | &nbsp; <span class="highlight" onclick="atomonelight()">Light</span>&nbsp; | &nbsp;<span class="highlight" onclick="atomonedark()">Dark</span> &nbsp; | &nbsp;<span class="highlight"><a href="code.php" target="_blank">Open in new tab</a></span>&nbsp; | &nbsp; <span class="highlight" onclick="copyToClipboard('toclipboard')">Copy to clipboard</span></h3>


            <pre><code id="toclipboard">          
<?php
$file = file_get_contents("build_appimage.sh");
$file_cleaned = htmlspecialchars($file, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5);
echo $file_cleaned;
?>
</code></pre>
        </section>

        <!-- Article 1 -->
        <!--
        <article>
            <h3>Heightlighted code</h3>
            <p>Heightlighted code using css and script from <a href="https://highlightjs.org/"
                    target="_blank">https://highlightjs.org</a>

        </article>
    -->
    </div> <!-- content 1 -->



    <!-- content 2 -->
    


    <!-- content 2 -->
    <footer>
    <div>&copy; Copyright 2023 - <?php echo date("Y"); ?>
            Ingemar Ceicer<br>
            programming@ceicer.com
        </div>
    </footer>

</div> <!-- container -->

</body>

</html>