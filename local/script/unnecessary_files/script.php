<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="../css/stilmall.css">
   
    <script src="../../js/highlight.min.js"></script>
    <script src="../../jquery/jquery.js"></script>
    <script>
        hljs.highlightAll();
    </script>

    <script>
        $(document).ready(
            function () {
                $("img").dblclick(function () {
                    this.requestFullscreen()
                })
            }

        );

            function copyToClipboard() {
                            var range = document.createRange();
                            range.selectNode(document.getElementById("toclipboard"));
                            window.getSelection().removeAllRanges(); // clear current selection
                            window.getSelection().addRange(range); // to select text
                            document.execCommand("copy");
                            window.getSelection().removeAllRanges();// to deselect
                            alert("The contents of the file have been copied to the clipboard.");
            }
                        

function atomonedark() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-dark.min.css">');
}

function atomonelight() {
        $('head').append('<link rel="stylesheet" type="text/css" href="../../css/atom-one-light.min.css">');
}
    </script>

    <title>Unnecessary files</title>
</head>

<div class="container">
    <header>
        <p>BASH script Unnecessary files</p>


    </header>
    <nav>
        <div class="btn-group">
            <a class="button" href="../../index.php">ceicer.eu</a>



        </div> <!-- btn-group -->
    </nav>
    <div class="content">
        <!-- content 1 -->
        <section class="smal">
            <h3> Script to find *.html and *.php files that have no internal links</h3>

            <p>To find "dead" files that are no longer in use.
            </p>
            <p>Searches a directory tree recursively.</p>
            <br>
            <h3>Example (Double click for full screen)</h3>
            <img src="images/deadlinks.png" alt="Dead links">


   
                    <h3>Download script</h3> 
                    <p><a href="../../bin/script/unnecessary_files.sh.tar.gz">unnecessary_files.sh.tar.gz</a></p>

        </section>


        <section class="big">

        <h3>unnecessary_files.sh &nbsp; | &nbsp; <span class="highlight" onclick="atomonelight()">Light</span>&nbsp; | &nbsp;<span class="highlight" onclick="atomonedark()">Dark</span> &nbsp; | &nbsp;<span class="highlight"><a href="code.php" target="_blank">Open in new tab</a></span>&nbsp; | &nbsp; <span class="highlight" onclick="copyToClipboard()">Copy to clipboard</span></h3>




<pre><code id="toclipboard">        
<?php
$file = file_get_contents("unnecessary_files.sh");
$file_cleaned = htmlspecialchars($file, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5);
echo $file_cleaned;

?>
</code></pre>
        </section>

        <!-- Article 1 -->
        <!--
        <article>
            <h3>Heightlighted code</h3>
            <p>Heightlighted code using css and script from <a href="https://highlightjs.org/"
                    target="_blank">https://highlightjs.org</a>

        </article>
    -->
    </div> <!-- content 1 -->



    <!-- content 2 -->
    <!--
    <div class="content">
        <section>
        <h3>Example (Double click for full screen)</h3>
       <img src="images/deadlinks.png" alt="Deadl links">
        </section>

        <section>
        content 2, section 2
        
       </section>


       <article>
       content 2, article

      </article>


    </div> 
-->
    <!-- content 2 -->
    <footer>
    <div>&copy; Copyright 2023 - <?php echo date("Y"); ?>
            Ingemar Ceicer<br>
            programming@ceicer.com
        </div>
    </footer>

</div> <!-- container -->

</body>

</html>