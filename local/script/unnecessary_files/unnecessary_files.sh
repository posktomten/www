#!/bin/bash
# Searches for files that lack internal links.
# Searches recursively from its position in the file system


# unnecessary_files.sh Copyright (C) 2023 Ingemar Ceicer
# https://ceicer.eu
# programming@ceicer.com

# unnecessary_files.sh is free software:
# you can redistribute it and/or modify
# it under the terms of the
# GNU General Public License as published by
# the Free Software Foundation, version 3.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

function htmlandphp()
{
 filesToCheck=$(find ${path} -type f -name "*.html")
 filesToCheck+=$(find ${path} -type f -name "*.php")
 filenames=(`echo $filesToCheck | sed 's/,/\n/g'`)

for i in "${!filenames[@]}"; do

          basnamn=$(basename -- "${filenames[$i]}")
          filename="${filenames[$i]}"
          haslink="1"

                for i in "${!filenames[@]}"; do

                #Searches for the text string
                #"filename" in the file
                if grep -Fqs "$basnamn" "${filenames[$i]}"

                then

                haslink="0"
                    break
               fi

              done

                if [ "$haslink" -eq "1" ]; then
                    #Paths to files without links
                    #end up in an array
                    arrVar+=("$filename")
                fi

done

echo
echo -----------------------------------------------------------
echo These files have no internal links
for value in "${arrVar[@]}"
do
     echo $value
done

echo "${#arrVar[@]}" files
echo
exit
}

PS3='Pleas select the root to start the recursive search from: '
#Position in the file system
start=$(echo Recursively from `pwd`)

options=("${start} (Current path)" "I choose" "Quit")
select opt in "${options[@]}"; do
    case $opt in
    "${start} (Current path)")
        echo "You chose choice $REPLY which is $opt"
        htmlandphp
        path= $(echo `pwd`)
        break
        ;;
    "I choose")
        echo "You chose choice $REPLY which is $opt"
        echo -n "Enter path: "
        read path
        htmlandphp
        break
        ;;
    "Quit")
        break
        ;;
    *) echo "invalid option $REPLY" ;;
    esac
done

echo -----------------------------------------------------------


