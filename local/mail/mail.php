<?php
session_start();
$_SESSION["name"] = $_POST["Name"];
$_SESSION["email"] = $_POST["Email"];
$_SESSION["subject"] = $_POST["Subject"];
$_SESSION["body"] = $_POST["Body"];
$recipient = $_POST["recipient"];
$mailfrom=$_POST['mailfrom'];
$ready_send = true;

if ($_SERVER["REQUEST_METHOD"] !== "POST") {
    exit("Direct access not supported.");
}
$failure="";

if (isset($_POST["required"]) && !empty($_POST["required"])) {
    $missing_required_fields = false;
    $required = explode(",", $_POST["required"]);

    foreach ($required as $req) {
        if (!isset($_POST[$req]) || empty($_POST[$req])) {

            $failure .= "You have not filled in the required fields<br>";
            $ready_send = false;
            break;
        }
    }
   
    // check if email address is well-formed
    if (!filter_var($_POST["Email"], FILTER_VALIDATE_EMAIL)) {

        $ready_send = false;
        $failure .=  "Incorrect email address<br>";
    }

    // check if name is well-formed
    if (!preg_match("/^[a-zA-Z-' ]*$/", $_POST["Name"])) {
        $ready_send = false;
        $failure .=  "Incorrect name<br>";
    }

    if (trim($_POST["Body"]) == "") {
        $ready_send = false;
        $failure .= "You must enter something in the message field<br>";
    }
}

if (isset($_POST["Name"]) && !empty($_POST["Name"])) {
    $mail_from_name = "ceicer.eu: " . $_POST["Name"];
} else {
    $mail_from_name = "ceicer.eu: unknown";
}

if (isset($_POST["Email"]) && !empty($_POST["Email"])) {
    $mail_from = $_POST["Email"];
} else {
    $mail_from = "";
}

if (!empty($mail_from) and !empty($mail_from_name)) {
    $mail_sender ="=?UTF-8?B?" . base64_encode($mail_from_name) . "?= <" . $mail_from . ">";
} elseif (!empty($mail_from)) {
    $mail_sender = $mail_from;
} else {
    $mail_sender = "=?UTF-8?B?" . base64_encode($mail_from_name) . "?=";
}

if (isset($_POST["subject"]) && !empty($_POST["subject"])) {
    $mail_subject = $_POST["subject"];
} else {
    $mail_subject = "ceicer.eu form submission";
}

$now = date("Y-m-d H:i:s");
$message = "Mail from ceicer.eu.";
$message .= "\n\n";
$message .= "Time: ".$now."\n";
$message .= "Origin page: ".$mailfrom."\n";
$message .= "Server: ".$_POST['OS']."\n";
$message .= "User agent: ".$_POST['useragent']."\n\n";

foreach ($_POST as $key => $value) {

    if (
        in_array($key, [
            "Name",
            "Email",
            "Subject",
            "Body",

        ])
    ) {
        $message .= $key . ": " . $value . "\n";
    }


}



//$headers = "From: " .$_SESSION['email']."\r\n";
$headers = "From: ". $recipient."\r\n";
$headers .= "Reply-To: ". $recipient."\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/plain; charset=utf-8\r\n";
$headers .= "X-Priority: 3\r\n";
$headers .= "X-MSMail-Priority: Normal\r\n";
$headers .= "X-Mailer: php\r\n";
$headers .= "Content-Transfer-Encoding: base64";
$mail_subject = "=?UTF-8?B?" . base64_encode($mail_subject) . "?=";

if ($ready_send==true) {
    $mail_sent = mail(
        $recipient,
        $mail_subject,
        base64_encode($message),
        $headers
    );


    $name=$_SESSION["name"];
    unset($_SESSION["name"]);
    unset($_SESSION["email"]);
    unset($_SESSION["subject"]);
    unset($_SESSION["body"]);



    header("Location: " . $_POST["redirect"]."?name=". $name);
}  else {

    $email=$_SESSION["email"];
    $name=$_SESSION["name"];
    $subject=$_SESSION["subject"];
    $body=$_SESSION["body"];

    unset($_SESSION["name"]);
    unset($_SESSION["email"]);
    unset($_SESSION["subject"]);
    unset($_SESSION["body"]);

       header("Location: " . $_POST["redirect_error"]."?failure=".$failure."&email=".$email."&name=".$name."&subject=".$subject."&body=".$body);
}

?>
