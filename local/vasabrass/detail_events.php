<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

$id=$_POST['id'];
?>

<!DOCTYPE html>
<!--
ALLA EVENT LISTAS
MÖJLIGHET ATT REDIGERA OCH ATT TA BORT OCH LÄGGA TILL
-->
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    
    <link rel="stylesheet" type="text/css" href="css/detail_events.css">
    
 

    <title>Vasa Brass - Administration kalender</title>


</head>

<body>


 
<?php
require "../../vasabrass";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 

try {

       // ENDAST MEDLEMMAR
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

   $stmt = $conn->prepare("SELECT * FROM $tbevents WHERE _id='$id' LIMIT 1"); 
   $stmt->execute();
   $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
   echo "<div class=\"container\">";  
foreach ($data as $row) {

//
echo "<div class=\"content\">";
echo "<div class=\"event_members\">";
echo "<form action=\"save_details_event.php\" method=\"post\">";

    echo "<div>";
    echo "<label for=\"date\">Datum</label>"; 
        echo "<input type=\"text\" name=\"date\" id=\"date\" placeholder=\"YYYY-MM-DD\" value=\"".$row['_date']."\">";
    echo "</div>";  /* date */

    echo "<div>";
    echo "<label for=\"day_of_week\">Veckodag</label>"; 
        echo "<input type=\"text\" name=\"day_of_week\" id=\"day_of_week\" value=\"".$row['_day_of_week']."\">";
    echo "</div>";  /* day_of_week */


    echo "<div>";
    echo "<label for=\"time\">Veckodag</label>"; 
        echo "<input type=\"text\" name=\"time\" id=\"time\" value=\"".$row['_time']."\">";
    echo "</div>";  /* time */

    echo "<div>"; 
    echo "<label for=\"place\">Plats</label>"; 
        echo "<input type=\"text\" name=\"place\" id=\"place\" value=\"".$row['_place']."\">";
    echo "</div>";  /* place */

    echo "<div>";  
    echo "<label for=\"notes\">Noteringar</label>"; 
        echo "<textarea name=\"notes\" id=\"notes\"  rows=\"20\" cols=\"20\" placeholder=\"Notera att du måste skriva datum på formen ÅÅÅÅ-MM-DD, till exempel 2023-08-23.\">".$row['_notes']."</textarea>";
        echo "<input type=\"hidden\" name=\"id\" value=\"".$row['_id']."\">";
    echo "</div>";  /* notice */



//    }



  echo "<div class=\"ny\">";
  echo "<button type=\"submit\" class=\"run nyknapp\">Spara</button>";
  echo "</form>"; 
//
  echo "<form action=\"delete_events.php\" method=\"post\">";
  echo "<button type=\"submit\" class=\"run nyknapp\">Ta bort</button>";
  echo "<input type=\"hidden\" name=\"id\" value=\"".$row['_id']."\">";
  echo "</form>";

  echo "<form action=\"admin_events.php\" method=\"post\">";
  echo "<button type=\"submit\" class=\"run nyknapp\">Avbryt</button>";
  echo "</form>";

  echo "<form action=\"index.php\" method=\"post\">";
  echo "<button type=\"submit\" class=\"run nyknapp\">Startsidan</button>";
  echo "</form>";
  echo "</div>"; /* ny */
  echo "</div>"; // event_members
  echo "</div>";  /* content */

}

echo "</div>";  /* container */
} catch(PDOException $e) {
    echo "Error: FEL " . $e->getMessage();
}
  $conn = null;
?>
</body>
</html>