<?php
session_start();
?>
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/members.css">
    
    <script src="js/sorttable.js"></script>

    <title>Vasa Brass - Medlemmar</title>




</head>

<body> 

<header>
            <img class="logo" src="images/logo.jpg" alt="Logo">
            <div id="welcome">Välkommen till Vasa Brass</div>

        </header>
        <nav>
            <div class="btn-group">

                <a class="button" href="index.php">Hem</a>
                <a class="button" href="about_us.php">Om oss</a>
                <a class="button" href="members.php">Medlemmar</a>
                <a class="button" href="events.php">Kalender</a>
                <a class="button" href="https://soundcloud.com/user-274831219-500717936" target="_blank">Lyssna</a>
                <a class="button" href="photo_gallery.php">Fotogalleri</a>
                <a class="button" href="contact_us.php">Kontakta oss</a>
                <a class="button" href="https://www.facebook.com/vasabrass" target="_blank">Facebook</a>


            </div> <!-- btn-group -->
        </nav>
        
        <div class="container">



        
<?php

require "../../vasabrass";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 

 

try {

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // SQL Query
    // $stmt = $conn->query("SELECT _name, _musical_instrument  FROM $tbmembers ORDER BY _name ASC"); 
    $stmt = $conn->prepare("SELECT _name, _musical_instrument  FROM $tbmembers ORDER BY _name ASC");
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo "<table id=\"members\">"; 
    echo "<thead title=\"Klicka och sortera\"><tr>";   
    echo "<th onclick=\"sortTable(0)\">Name</th><th onclick=\"sortTable(1)\">Musikinstrument</th></tr>";
    echo "</thead>";
    echo "<tbody>";
    // iterate over rows
    foreach($data as $row) { 
     

        echo "<tr>";
        echo "<td>".$row['_name'] ."</td><td>". $row['_musical_instrument']."</td>";
        echo "</tr>";
    }
echo "</tbody>";
echo "</table>"; 

} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
  $conn = null;



  if(isset($_SESSION['logged_in'])){
    echo "<footer class=\"footer_loggin\">";
  } else {

    echo "<footer>";
  }
?>


            <div>Vasa Brass i sammarbete med<br>
                <img class="windcorp" src="images/windcorp.png" alt="windcorp"></div>
               
                <div>Webb av <a class="mailto" href="mailto:ingemar@ceicer.eu"> ingemar@ceicer.eu </a>  Copyright © 2023 Vasa Brass. All Rights Reserved.</div>
            
      <?php
            if(isset($_SESSION['logged_in'])){
                echo "<div>";
                echo "<form action=\"admin_members.php\" method=\"post\">"; 
                echo "<input type=\"submit\" value=\"Redigera\">";
                echo "</form> </div>";
            } 
      ?>
</footer>

</div> <!-- container-->



</body>
</html>