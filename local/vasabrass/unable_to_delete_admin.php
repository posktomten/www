<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

$name=$_GET['name'];
?>
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/detail.css">
    

    <title>Vasa Brass - Kan inte ta bort administratör</title>


</head>

<body>
<?php
echo "<div id=\"detail\">";
       echo "<form action=\"admin_members.php\" method=\"post\">";
       echo "<label for=\"usrname\">Omöjligt att ta bort ".$name. " som administratör. Det måste alltid finnas minst en administratör. Skapa en ny administratör innan du tar bort admistratören ".$name."  .</label>";
       echo "<input type=\"submit\" name=\"submit\" value=\"Ok\">";
       echo "</form>";
       echo "</div>";
?>
</body>
</html>
