<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

?>
<!DOCTYPE html>
<!--
ALLA EVENT LISTAS
MÖJLIGHET ATT REDIGERA OCH ATT TA BORT OCH LÄGGA TILL
-->
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
    <link rel="stylesheet" type="text/css" href="css/detail_events.css">
    
    
    <title>Vasa Brass - Administration kalender</title>


<title>Vasa Brass - Administration</title>
</head>

<body>


 
<?php
require "../../vasabrass";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



try {

  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  // SQL Query
  // $stmt = $conn->query("SELECT _date, _time, _place, _notes FROM $tbevents ORDER BY _date DESC"); 
  $stmt = $conn->prepare("SELECT * FROM $tbevents ORDER BY _date DESC");
  $stmt->execute();
  $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo "<div class=\"container\">";
  /* *** */
  $rowcount = $stmt->rowCount();

  if ($rowcount < 1){
    echo "<div class=\"content\">";
    echo "<div class=\"attention\">Det finns inga kalenderposter</div>"; 
  }
 else {
 
  foreach ($data as $row) {

    //
    echo "<div class=\"content\">";
    echo "<div class=\"event_members\">";
    echo "<form action=\"detail_events.php\" method=\"post\">";
    
        echo "<div>";
        echo "<label for=\"date\">Datum</label>";
        echo "<input type=\"text\" id=\"date\" name=\"date\" value=\"".$row['_date']."\">";
        echo "</div>";  /* date */
    
         
        echo "<div>";
        echo "<label for=\"day_of_week\">Veckodag</label>";
        echo "<input type=\"text\" id=\"day_of_week\" name=\"day_of_week\"  value=\"".$row['_day_of_week']."\">";
        echo "</div>";  /* day_of_week */
    
        echo "<div>";
        echo "<label for=\"time\">Tid</label>"; 
        echo "<input type=\"text\" id=\"time\" name=\"time\" value=\"".$row['_time']."\">";
        echo "</div>";  /* time */
    
        echo "<div>"; 
        echo "<label for=\"place\">Plats</label>";  
        echo "<input type=\"text\" id=\"place\" name=\"place\" value=\"".$row['_place']."\">";
        echo "</div>";  /* place */
    
 
        echo "<div>"; 
        echo "<label for=\"notes\">Plats</label>";  
        echo "<textarea id=\"notes\" name=\"notes\">".$row['_notes']."</textarea>";
        echo "</div>";  /* notice */
    
    

      //echo "</div>"; // content
    
    

echo "<form action=\"detail_events.php\" method=\"post\">";
echo "<input type=\"hidden\" name=\"id\" value=\"".$row['_id']."\">";
echo "<button type=\"submit\"  class=\"run nyknapp\">Redigera</button>";
echo "</form>";


  echo "</div>";  /* content */
  echo "</div>"; // event_members
  
  }
}
  echo "<div class=\"ny\">";

  echo "<form action=\"new_event.php\" method=\"post\">";
  //echo "<input type=\"hidden\" name=\"id\" value=\"".$row['_id']."\">";
  echo "<button type=\"submit\"  class=\"run nyknapp\">Ny kalenderpost</button>";
  echo "</form>";
 


  echo "<form action=\"index.php\" method=\"post\">";
  echo "<button type=\"submit\"  class=\"run nyknapp\">Startsidan</button>";
  echo "</form>";

  echo "</div>"; // ny

  echo "</div>"; // container


} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
 

?>
</body>
</html>