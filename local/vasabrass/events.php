<?php
session_start();
?>
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/detail_events.css">
    
    
    <title>Vasa Brass - Kalender</title>


</head>

<body>

<header>
            <img class="logo" src="images/logo.jpg" alt="Logo">
            <div id="welcome">Välkommen till Vasa Brass</div>

 

        </header>
        <nav>
            <div class="btn-group">

                <a class="button" href="index.php">Hem</a>
                <a class="button" href="about_us.php">Om oss</a>
                <a class="button" href="members.php">Medlemmar</a>
                <a class="button" href="events.php">Kalender</a>
                <a class="button" href="https://soundcloud.com/user-274831219-500717936" target="_blank">Lyssna</a>
                <a class="button" href="photo_gallery.php">Fotogalleri</a>
                <a class="button" href="contact_us.php">Kontakta oss</a>
                <a class="button" href="https://www.facebook.com/vasabrass" target="_blank">Facebook</a>


            </div> <!-- btn-group -->
        </nav>
     
  
        
<?php

require "../../vasabrass";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 

 

try {

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // SQL Query
    // $stmt = $conn->query("SELECT _date, _time, _place, _notes FROM $tbevents ORDER BY _date DESC"); 
    $stmt = $conn->prepare("SELECT * FROM $tbevents ORDER BY _date DESC");
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo "<div class=\"container\">";
    /* *** */
    foreach ($data as $row) {

    //
    echo "<div class=\"content\">";
    
  
      echo "<div class=\"event_members\">";
      echo "<form>";
        echo "<div>";
        echo "<label for=\"date\">Datum</label>"; 
        echo "<input type=\"text\" id=\"date\" name=\"date\" value=\"".$row['_date']."\">";
        echo "</div>";  /* date */
    
         
        echo "<div>";
        echo "<label for=\"day_of_week\">Veckodag</label>"; 
        echo "<input type=\"text\" id=\"day_of_week\" value=\"".$row['_day_of_week']."\">";
        echo "</div>";  /* day_of_week */
    
        echo "<div>";
        echo "<label for=\"time\">Klockslag</label>"; 
        echo "<input type=\"text\" id=\"time\" value=\"".$row['_time']."\">";
        echo "</div>";  /* time */
    
        
        echo "<div>";
        echo "<label for=\"place\">Plats</label>";  
        echo "<input type=\"text\" id=\"place\" value=\"".$row['_place']."\">";
        echo "</div>";  /* place */
    
 
        echo "<div>";
        echo "<label for=\"notes\">Noteringar</label>"; 
        echo "<textarea id=\"notes\">".$row['_notes']."</textarea>";
        echo "</div>";  /* notice */
        echo "</form>";
    
        echo "</div>"; // event_members
      echo "</div>"; // content
    
    }
    echo "</div>"; // container
} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
  $conn = null;


  if(isset($_SESSION['logged_in'])){
    echo "<footer class=\"footer_loggin\">";
  } else {

    echo "<footer>";
  }

?>




            <div>Vasa Brass i sammarbete med<br>
                <img class="windcorp" src="images/windcorp.png" alt="windcorp"></div>
               
            <div>Webb av <a class="mailto" href=ingemar@ceicer.eu> ingemar@ceicer.eu </a>  Copyright © 2023 Vasa Brass. All Rights Reserved.</div>
            
<?php
            if(isset($_SESSION['logged_in'])){
                echo "<div>";
                echo "<form action=\"admin_events.php\" method=\"post\">"; 
                echo "<input type=\"submit\" value=\"Redigera\">";
                echo "</form> </div>";
            } 
?>

            
           
</footer>

<br>




</body>
</html>
