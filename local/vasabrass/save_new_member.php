<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

if (isset($_POST['cancel'])){
    header("Location: admin_members.php");
    exit;
  }

  require "../../vasabrass";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

  $name=$_POST['name'];
  $musical_instrument=$_POST['musical_instrument'];

  try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "INSERT INTO $tbmembers (_name, _musical_instrument) VALUES (?,?)";
    $stmt= $conn->prepare($sql);
    $stmt->execute([$name, $musical_instrument]);
      
    //echo "Record updated successfully";
 
} catch(PDOException $e) {
    //echo "Error: " . $e->getMessage();
}
$conn = null;

header('Location: admin_members.php');
//exit;
?>