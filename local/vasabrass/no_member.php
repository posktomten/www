<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}


?>
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/detail.css">
    
    <title>Vasa Brass - Användarnamnet är upptaget</title>

</head>

<body>
<?php
echo "<div id=\"detail\">";
       echo "<form action=\"admin_members.php\" method=\"post\">";
       echo "<label for=\"submit\">Det finns ingen användare som kan bli administratör.</label>";
       echo "<input type=\"submit\" name=\"submit\" value=\"Ok\">";
       echo "</form>";
       echo "</div>";
?>
</body>
</html>
