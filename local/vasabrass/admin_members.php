<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

?>
<!DOCTYPE html>
<!--
ALLA MEDLEMMAR OC ADMINISTRATÖRER LISTAS
MÖJLIGHET ATT REDIGERA OCH ATT TA BORT OCH LÄGGA TILL
-->
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/gradient.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
    <link rel="stylesheet" type="text/css" href="css/admin_members.css">
     

    <title>Vasa Brass - Administration medlemmar</title>


</script> 

</head>

<body>


<?php

require "../../vasabrass";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 

 

try {

       // ENDAST MEDLEMMAR
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

   $stmt = $conn->prepare("SELECT $tbmembers._id, $tbmembers._name, $tbmembers._musical_instrument FROM $tbmembers  ORDER BY _name ASC"); 
   $stmt->execute();
   $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

  
   echo "<div class=\"content\">";
   echo "<div class=\"attention stor\">Medlemmar</div>";
   // iterate over rows
  
   foreach($data as $row) { 

echo "<form action=\"detail_members.php\" method=\"post\">";
echo "<div class=\"post\">";
echo "<div class=\"attention\">Namn</div>";
echo "<div class=\"post_detail\">".$row['_name']."</div>";
echo "<div class=\"attention\">Musikinstrument</div>";
echo "<div class=\"post_detail\">".$row['_musical_instrument']."</div>";
echo "<input type=\"hidden\" name=\"id\" value=\"".$row['_id']."\">";
echo "<input type=\"submit\" class=\"run\" value=\"Redigera\">";
echo "</div>"; // post
echo "</form>";

   }
echo "</div>"; // content
  
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;





   // ENDAST ADMINISTRATÖRER

   
   try{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT $tbmembers._id, $tbmembers._name, $tbadministrators._usrname FROM $tbmembers INNER JOIN $tbadministrators ON $tbadministrators._members_id=$tbmembers._id ORDER BY _name ASC"); 
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo "<div class=\"content\">";

       echo "<div class=\"attention stor\">Administratörer</div>";


    // iterate over rows

    foreach($data as $row) { 

echo "<form action=\"detail_administrators.php\" method=\"post\">";
echo "<div class=\"post\">";
echo "<div class=\"attention\">Namn</div>";
echo "<div class=\"post_detail\">".$row['_name']."</div>";
echo "<div class=\"attention\">Användarnamn</div>";
echo "<div class=\"post_detail\">".$row['_usrname']."</div>";
echo "<input type=\"hidden\" name=\"id\" value=\"".$row['_id']."\">";
echo "<input type=\"hidden\" name=\"name\" value=\"".$row['_name']."\">";
echo "<input type=\"submit\" class=\"run\" value=\"Redigera\">";
echo "</div>"; // post
echo "</form>";

    }
   


echo "</div>"; // content




} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
  $conn = null;


  echo "<div class=\"ny\">";
  echo "<form action=\"new_member.php\" method=\"post\">";
  echo "<button type=\"submit\"  class=\"run nyknapp\">Ny medlem</button>";
  echo "</form>";
  echo "<form action=\"select_member_for_administrator.php\" method=\"post\">";
  echo "<button type=\"submit\"  class=\"run nyknapp\">Ny administratör</button>";
  echo "</form>";
  echo "<form action=\"index.php\" method=\"post\">";
  echo "<button type=\"submit\"  class=\"run nyknapp\">Startsidan</button>";
  echo "</form>";
  echo "</div>"; 

?>


</body>
</html>