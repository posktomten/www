<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

$name=$_POST['name'];
$adm_id=$_POST['adm_id'];
// OM INTE ADMINISTRATÖREN LÄNGRE SKA VARA ADMINISTRATÖR
// KOLLA SÅ ATT DET INTE ÄR DEN SISTA.

require "../../vasabrass";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("SELECT * FROM $tbadministrators");
      $stmt->execute(); // Execute the prepared statement
      $rowcount = $stmt->rowCount();
    
      } catch(PDOException $e) {
        //echo "Error: " . $e->getMessage();
      }
      $conn = null;
    
      
      if ($rowcount < 2){
        // KAN INTE TA BORT MEDLEMMEN DÅ MEDLEMMEN
        // ÄR ENSAM ADMINISTRATÖR
          header("Location: unable_to_delete_admin.php?name=".$name);
          exit;
      }
     
      try {
  
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("DELETE FROM $tbadministrators WHERE _id = '$adm_id'");
         // use exec() because no results are returned
         $stmt->execute();
         //echo "Record deleted successfully";
      
       } catch(PDOException $e) {
         echo $sql . "<br>" . $e->getMessage();
       
       }
       
       $conn = null;
       header("Location: admin_members.php");
       ?>