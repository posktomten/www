<?php
session_start();

function validatePasswd($passwd)
{
    if   (preg_match('/^(?=.*[a-zA-Z0-9])(?=.*[!@#$%^&*()_+\\-=\\[\]{};:\'",.<>\/?]).{8,}$/', $passwd))   {
         return true;
    }
    
    return false;
}

function validateUsrname($usrname) {
    // Använd ^ för att matcha början av strängen
    // Använd \S för att matcha icke-vita tecken
    // Använd {3,} för att säkerställa att det är minst 3 tecken
    // Använd $ för att matcha slutet av strängen
    return preg_match('/^\S{3,}$/', $usrname);
}


?>