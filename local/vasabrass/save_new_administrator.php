<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

require "../../vasabrass";
require_once("validate.php");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$name=$_POST['name'];

if (isset($_POST['cancel'])){
    header("Location: admin_members.php");
    exit;
  }
  $usrname=$_POST['usrname'];
  $passwd=$_POST['passwd'];
  $id=$_POST['id'];
  $name=$_POST['name'];
  
// KOLLAR ATT LÖSENORDET ÄR OK
if (!validatePasswd($passwd)){
header("Location: passwd_bad.php?name=".$name);

exit;
}
if (!validateUsrname($usrname)){
  header("Location: usrname_bad.php?name=".$name);
exit;
}

// KOLLAR ATT INTE ANVÄNDARNAMNET ÄR UPPTAGET

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT _usrname FROM $tbadministrators WHERE _usrname='$usrname'");
  $stmt->execute(); // Execute the prepared statement
  $rowcount = $stmt->rowCount();
  } catch(PDOException $e) {
    //echo "Error: " . $e->getMessage();
  }
  $conn = null;
  
  
  if ($rowcount > 0){
  
   header("Location: usrname_is_occupied.php?name=".$usrname);
   exit;
  }
  require "../../vasabrass";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 
   // SLUT KOLLAR ATT INTE ANVÄNDARNAMNET ÄR UPPTAGET
   $options = ['cost' => 12];
   $encrypted_passwd=password_hash ($password, PASSWORD_BCRYPT, $options);;
  //$encrypted_passwd = PASSWORD_HASH($password, PASSWORD_DEFAULT);
  
      try {
          $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
          // set the PDO error mode to exception
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
          // Korrekt sätt att bygga SQL-frågan
          $sql = "INSERT INTO $tbadministrators (_usrname, _passwd, _members_id) VALUES (?,?,?)";
          $stmt=$conn->prepare($sql);
          $stmt->execute([$usrname, $encrypted_passwd, $id]);

         // echo "Record updated successfully";
      
      } catch(PDOException $e) {
         // echo "Error: " . $e->getMessage();
        
      }
      $conn = null;
    
      header("Location: admin_members.php");
 
  
  ?>
