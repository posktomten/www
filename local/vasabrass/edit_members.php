<?php
session_start();

if(!isset($_SESSION['logged_in'])){
  header("Location: not_allowed.php");
  exit;
}

$id = $_POST['id'];
$name = $_POST['name'];
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (isset($_POST['cancel'])){
  header("Location: admin_members.php");
  exit;
}
  if (isset($_POST['del'])){
    require "../../vasabrass";

  // KOLLAR OM ADMINISTRATOR
 
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT _members_id FROM $tbadministrators WHERE _members_id=? LIMIT 1"); 
  $stmt->execute([$id]); 
  $row = $stmt->fetch();
  // OM $row är true hittas en rad, medlemmen är administratör
  if ($row) {

  

   // KOLLA SÅ ATT DET INTE ÄR SISTA ADMINISTRATORN
   require "../../vasabrass";
   try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT * FROM $tbadministrators");
  $stmt->execute(); // Execute the prepared statement
  $rowcount = $stmt->rowCount();
  } catch(PDOException $e) {
    //echo "Error: " . $e->getMessage();
  }
  
  
  if ($rowcount < 2){
    // KAN INTE TA BORT MEDLEMMEN DÅ MEDLEMMEN
    // ÄR ENSAM ADMINISTRATÖR
   header("Location: unable_to_delete_admin.php?name=".$name);
   exit;
  
  $conn = null;
  }
   else {
      // OFARLIGT ATT TA BORT MEDLEMMEN DÅ DET
      // FINNS FLER ADMINISTRATÖRER
  try {
  
   $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
   // set the PDO error mode to exception
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $stmt = $conn->prepare("DELETE $tbmembers, $tbadministrators FROM $tbmembers JOIN $tbadministrators ON $tbadministrators._members_id=$tbmembers._id WHERE $tbmembers._id = '$id';");
    // use exec() because no results are returned
    $stmt->execute();
    //echo "Record deleted successfully";
 
  } catch(PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
  
  }
  
  $conn = null;
   }
  // END DELETE OM ADMINISTRATOR

  } 
  // MEDLEMMEN ÄR INTE ADMINISTRATOR
  // OCH KAN TAS BORT
  else {

    try {
  
      $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
      // set the PDO error mode to exception
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("DELETE FROM $tbmembers WHERE $tbmembers._id = '$id'");
       // use exec() because no results are returned
       $stmt->execute();
       //echo "Record deleted successfully";
    
     } catch(PDOException $e) {
       echo $sql . "<br>" . $e->getMessage();
     
     }
     
     $conn = null;
  }




}
// END DELETE MEDLEM
// BEGIN EDIT MEDLEM

require "../../vasabrass";




$id = $_POST['id'];
$name = $_POST['name'];
$musical_instrument = $_POST['musical_instrument'];

// ! NOT DELETE


try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Korrekt sätt att bygga SQL-frågan
      $sql = "UPDATE $tbmembers SET _name='$name', _musical_instrument='$musical_instrument' WHERE _id='$id'";
    $stmt = $conn->prepare($sql);
    $conn->exec($sql);
    //echo "Record updated successfully";
 
} catch(PDOException $e) {
    //echo "Error: " . $e->getMessage();
}
$conn = null;

header('Location: admin_members.php');
//exit;

?>
