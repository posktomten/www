<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">


    <meta name="keywords" content="posktomten, streamCapture2, streaming, download, television">
    <meta name="description" content="Download from SVTPlay and many other streaming service">
    <meta name="author" content="Ingemar Ceicer">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <title>streamCapture2 CLI gränssnitt</title>
</head>
<body>
  <?php
  $file = file_get_contents("help.txt");
  $file_cleaned = htmlspecialchars($file, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5);
  echo "<code><pre>";
  echo $file_cleaned;
  echo "</pre></code>";
  ?>

</body>
</html>
