<?php
session_start();
if ($_GET['lang']=="en_US") {
require_once("lang_en_US");
}
else if ($_GET['lang']=="it_IT") {
require_once("lang_it_IT");
}
else if ($_GET['lang']=="sv_SE") {
require_once("lang_sv_SE");
}
else {
require_once("lang_en_US");
}

Arch Linux 20220510 (64-bit) AppImage
Bodhi Linux 6.0 (64-bit) AppImage
BionicPup32 8.0 (64-bit) AppImage
Fedora 32, Workstation Edition (64-bit) AppImage
Fedora 34, Workstation Edition (64-bit) AppImage
KDE neon 20220505 (64-bit) AppImage
KDE neon 5.23.5 (64-bit) AppImage
Linuxmint 20 cinnamon (64-bit) AppImage
Lubuntu 19.10 (64-bit) AppImage
Lubuntu 20.10 (64-bit) AppImage
Lubuntu 21.10 (64-bit) AppImage
Lubuntu 22.04 (64-bit) AppImage
Manjaro-xfce 20.0.3-200606-linux56 (64-bit) AppImage
MX Linux 18.03 (64-bit) AppImage
MX Linux 23.01 (32-bit) AppImage
Peppermint 10 (32- and 64-bit) AppImage
Q4OS 3.11 (64-bit) AppImage
Salix 14.2.1 (64-bit) AppImage
Slackel 7.5 (32-bit) AppImage
Ubuntu 18.04 (32- and 64-bit) AppImage
Ubuntu 20.04 (64-bit) AppImage
Ubuntu 22.04 (64-bit) AppImage
Ubuntu 23.10 (64-bit) AppImage
Windows 7 (64-bit) Install, Portable
Windows 8.1 (64-bit) Install, Portable
Windows 10 (32- and 64-bit) Install, Portable
Windows 11 (64-bit) Install, Portable
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="fonts.css">
  <link rel="stylesheet" type="text/css" href="stilmall.css">
  <title><?php echo $lang['streamCapture2 Manual']; ?></title>

  <script>
  function clearAll() {

    document.getElementById('name').value='';
    document.getElementById('email').value='';
    document.getElementById('body').value='';
    document.getElementById('subject')[0].selected=true;
  }
  </script>

</head>

<!-- <body onload="openCity(event, 'manual')"> -->
<body>


  <div id="container">
    <!-- <div id="manual" class="tabcontent"> -->
      <!-- Manualen -->
      <h2><?php echo $lang['streamCapture2 Manual']; ?></h2>

      <div class="languagecontainer">
        <div class="language">
          <img class="noshadow" src="images/en_US/english.png" alt="English">
          <a class="noshadowling" href="index.php?lang=en_US#container">In English, Please!</a>
        </div>
        <div class="language">
          <img class="noshadow" src="images/it_IT/italian.png" alt="Italiano">
          <a class="noshadowling" href="index.php?lang=it_IT#container">In italiano, grazie!</a>
        </div>

        <div class="language">
          <img class="noshadow" src="images/sv_SE/swedish.png" alt="Swedish">
          <a class="noshadowling" href="index.php?lang=sv_SE#container">På svenska, tack!</a>
        </div>
      </div><!-- languagecontainer -->

      <span class="kursiv"><?php echo $lang['Many thanks to bovirus for the Italian translation.']; ?></span>

     <div class="margintop"><a href="#form"><?php echo $lang['Contact me']; ?></a></div>

      <p><?php echo $lang['Pleas visit']; ?>
     |  <a href="https://gitlab.com/posktomten/streamcapture2/wikis/home" target="_blank"><?php echo $lang['Website']; ?></a> 

     <?php  if ($_GET['lang']=="sv_SE") {  ?>
     
       |  <a href="https://ceicer.eu/streamcapture2/index.php" target="_blank"><?php echo $lang['sv_Website']; ?></a> 
  
       <?php } ?>


      |  <a href="https://gitlab.com/posktomten/streamcapture2/-/wikis/Manual" target="_bank"><?php echo $lang['video']; ?></a> 

       
        
      |  <a href="https://gitlab.com/posktomten/streamcapture2" target=" blank"><?php echo $lang['sourcecode']; ?></a> 

     |  <a href="https://svtplay-dl.se/" target=" blank"><?php echo $lang['svtplay-dl']; ?></a> 

      |  <a href="https://github.com/spaam/svtplay-dl/issues" target=" blank"><?php echo $lang['svtplay-dl_issues']; ?></a> |

      </p>

      <div class="download">
        <div class="download_item">
          <h3><?php echo $lang['Download a single episode']; ?></h3>
          <ol>
            <li><?php echo $lang['Find a web page that streams video']; ?></li>
            <li><?php echo $lang['Copy the page address']; ?></li>
            <li><?php echo $lang['Paste into streamCapture2']; ?></li>
            <li><?php echo $lang['Click Search']; ?></li>
            <!-- <li> --> <?php //echo $lang['Choose quality']; ?> <!-- </li> -->
            <li><?php echo $lang['Click Download']; ?></li>
          </ol>
        </div> <!-- download item -->

<div class="download_item">
    <h3><?php echo $lang['Download multiple episodes']; ?></h3>
<ol>
  <li><?php echo $lang['Find a web page that streams video']; ?></li>
  <li><?php echo $lang['Copy the page address']; ?></li>
  <li><?php echo $lang['Paste into streamCapture2']; ?></li>
  <li><?php echo $lang['Click Search']; ?></li>
  <li><?php echo $lang['Click add to Download list']; ?></li>
  <li><?php echo $lang['Click Download all']; ?></li>
</ol>
</div> <!-- download item -->

        <div class="download_item">
          <h3><?php echo $lang['Download all episodes']; ?></h3>
          <ol>
            <li><?php echo $lang['Find a web page that streams video']; ?></li>
            <li><?php echo $lang['Copy the page address']; ?></li>
            <li><?php echo $lang['Paste into streamCapture2']; ?></li>
            <li><?php echo $lang['Click all Episodes, List all Episodes']; ?></li>
            <li><?php echo $lang['Click All Episodes, Direct Download of all Episodes']; ?></li>
          </ol>
        </div> <!-- download item -->
      </div> <!-- download -->

      <!-- help.png -->
      <img src="images/<?php echo $lang['language']; ?>/help.png" alt="Help" id="help">
      <p class="kursiv"><?php echo $lang['If you hold the mouse over...']; ?></p>

      <!-- selectbitrate.png -->
      <img src="images/<?php echo $lang['language']; ?>/selectbitrate.png" alt="Select bite rate" id="selectbitratet">
      <h3><?php echo $lang['Select the quality...']; ?></h3>

      <ol>
        <li><?php echo $lang['Select bite rate...']; ?></li>
		<li><?php echo $lang['Resolution...']; ?></li>
		<li><?php echo $lang['Here you specify how much...']; ?></li>
        <li><?php echo $lang['If the download requires login...']; ?></li>
        <li><?php echo $lang['If you have not let the program save...']; ?></li>
      </ol>

      <!-- downloadlist.png -->
      <img src="images/<?php echo $lang['language']; ?>/downloadlist.png" alt="Download List" id="downloadlist">
      <p class="kursiv"><?php echo $lang['If you let svtplay-dl choose method...']; ?></p>
      <h3><?php echo $lang['Download List']; ?></h3>
      <p><?php echo $lang['The list where you can save what...']; ?></p>
      <ol>
        <li><?php echo $lang['The address...']; ?></li>
        <li><?php echo $lang['The method to be used']; ?> &quot;hls&quot;.</li>
        <li><?php echo $lang['Quality (bit rate)']; ?> &quot;4268&quot;.</li>
        <li><?php echo $lang['npassword if...']; ?></li>
        <li><?php echo $lang['ysub means that subtitles...']; ?></li>
        <li><?php echo $lang['nst if no cookie is to be used...']; ?></li>
        <li><?php echo $lang['Permitted deviation...']; ?></li>
		    <li><?php echo $lang['Resolution list']; ?></li>
      </ol>

      <!-- Dolby Vision 4K-->
      <h3>Dolby Vision 4K</h3>
      <?php echo $lang['Dolby Vision 4K']; ?>

      <h3><?php echo $lang['Problem solving']; ?></h3>
      <p><?php echo $lang['If you are using the Windows...']; ?></p>
    <!-- </div> -->
    <!-- Slut manualen -->

   <!-- form -->

   <div class="languagecontainer">
     <div class="language">
       <img class="noshadow" src="images/en_US/english.png" alt="English">
       <a class="noshadowling" href="index.php?lang=en_US#form">In English, Please!</a>
     </div>
     <div class="language">
       <img class="noshadow" src="images/it_IT/italian.png" alt="Italiano">
       <a class="noshadowling" href="index.php?lang=it_IT#form">In italiano, grazie!</a>
     </div>

     <div class="language">
       <img class="noshadow" src="images/sv_SE/swedish.png" alt="Swedish">
       <a class="noshadowling" href="index.php?lang=sv_SE#form">På svenska, tack!</a>
     </div>
   </div><!-- languagecontainer -->

      <div id="form" class="download">


        <div class="download_item">

          <h2><?php echo $lang['streamCapture2 contact form']; ?></h2>

          <form name="form" method="post" action="../mail/mail.php" accept-charset="utf-8">
            <input type="hidden" name="required" value="email,name">
              <input type="hidden" name="redirect" value="../help-v1.0/index.php?lang=<?php echo $lang['language']; ?>#form">
            <p>
              <?php echo $lang['Fill out this form...']; ?><br>
              *<?php echo $lang['Required']; ?>
            </p>
<?php


// From "lang xx_XX"
echo vadUt();

?>

<p>
    <label>
      <?php echo $lang['Name']; ?>*<br>
   </label>
      <input type="text" size="25"  maxlength="60" name="name" id="name" value="<?php echo $_SESSION['name']; ?>">
</p>

<p>
    <label>
      <?php echo $lang['E-mail']; ?>*<br>
      </label>
      <input type="text" size="25" maxlength="60" name="email" id="email" value="<?php echo $_SESSION['email']; ?>">
</p>

<!-- <p> -->
    <label>
      <?php echo $lang['Heading']; ?>
    </label>
<!-- </p> -->
      <?php $_SESSION['subject'] = $_POST['subject'];
      ?>
<br>

      <select name="subject" id="subject" size="4">

      <?php if ($_SESSION['subject'] == "bug") { ?>
         <option value="bug" selected><?php echo $lang['Bug report']; ?></option>
      <?php } else { ?>
          <option value="bug"><?php echo $lang['Bug report']; ?></option>
       <?php } ?>

       <?php if ($_SESSION['subject'] == "help") { ?>
          <option value="help" selected><?php echo $lang['Help']; ?></option>
       <?php } else { ?>
           <option value="help"><?php echo $lang['Help']; ?></option>
        <?php } ?>

      <?php if ($_SESSION['subject'] == "mypoint") { ?>
         <option value="mypoint" selected><?php echo $lang['My point']; ?></option>
      <?php } else { ?>
          <option value="mypoint"><?php echo $lang['My point']; ?></option>
       <?php } ?>

       <?php if ($_SESSION['subject'] == "other") { ?>
          <option value="other" selected><?php echo $lang['Other']; ?></option>
       <?php } else { ?>
           <option value="other"><?php echo $lang['Other']; ?></option>
        <?php } ?>

      </select>



  <p>
        <label>
          <?php echo $lang['Message']; ?>*<br>
          <textarea maxlength="8000" name="body" id="body"><?php echo $_SESSION['body']; ?></textarea>
        </label>
  </p>
  <p>
        <input type="submit" value="<?php echo $lang['Send']; ?>">
        <input type="button" value="<?php echo $lang['Clear']; ?>" onclick="clearAll()">
  </p>
      </form>


      <div><a href="#container"><?php echo $lang['Up']; ?></a></div>

      <!-- <div class="download_item"> -->
      </div>

    </div> <!-- id="form" -->

</div><!-- container -->

</body>
</html>
