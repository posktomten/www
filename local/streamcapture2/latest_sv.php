<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/stilmall.css">
    <script src="../js/sorttable.js"></script>
    <title>streamCapture2</title>

<script>
    function comment(id){
        document.getElementById("comment").innerHTML="<p>"+comments[id]+"</p>" ;
  }

</script>



</head>
<body>
<div class="container">
<header>
    <img class="appicon" src="images/appicon.png" alt="streamCapture2 ikon">
        <p>streamCapture2</p>
       <!-- <img class="appicon" src="images/appicon.png" alt="streamCapture2 ikon"> -->

    </header>
    <nav>
    <div class="btn-group">
            <a class="button" href="../index.php">ceicer.eu</a>
            <a class="button" href="index.php">Sida 1</a>
            <a class="button" href="more.php">Sida 2</a>
            <a class="button" href="latest_sv.php">Ladda ner</a>
            <a class="button" href="http://bin.ceicer.com/streamcapture2/bin/" target="_blank">Ladda ner BETA</a>
            <a class="button" href="latest.php">In English</a>
            <a class="button" href="https://gitlab.com/posktomten/streamcapture2" target="_blank">Källkod</a>
            <a class="button" href="https://gitlab.com/posktomten/streamcapture2/-/wikis/home" target="_blank">Wiki (engelska)</a>
            <a class="button" href="https://gitlab.com/posktomten/streamcapture2/-/raw/master/LICENSE?ref_type=heads" target="_blank">Licens (engelska)</a>
            <a class="button" href="https://gitlab.com/posktomten/streamcapture2/-/blob/master/code/txt/CHANGELOG_sv_SE" target="_blank">Historik</a>


        </div> <!-- btn-group -->
    </nav>


<section id="comment" class="comment">
<h2>Klicka på raderna i tabellen så får du information om versionerna.</h2>
</section>

          
<?php

require "../../streamcapture2";


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 

$rad=0;

try {

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT * FROM $tbstreamcapture2 ORDER BY _Operating_system, _Streamcapture2 ASC");
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
   
    /* *** */
    echo "<div class=\"scroll-content\">";
    echo "<div class=\"table_background\"><table id=\"members\">";
    echo "<tr><th onclick=\"sortTable(0)\">Operativsystem</th><th onclick=\"sortTable(1)\">Bitar</th><th onclick=\"sortTable(2)\">Python</th><th onclick=\"sortTable(3)\">Qt</th><th onclick=\"sortTable(4)\">Kompilator</th><th onclick=\"sortTable(5)\">Kompilatorversion</th><th onclick=\"sortTable(6)\">GLIBC</th><th onclick=\"sortTable(7)\">streamCapture2</th><th onclick=\"sortTable(8)\">Filtyp</th><th onclick=\"sortTable(9)\">FFmpeg</th><th onclick=\"sortTable(10)\">Länk</th><th onclick=\"sortTable(11)\">Länk</th></tr>";
    $comment=array();
  
    $ffmpeginc="";
    foreach ($data as $row) {
        array_push($comment, $row['_comment_sv']);
  
        if (trim($row['_FFmpeg_included'])==='None'){
            $ffmpeginc="Ingen";
        }
   
        else {
            $ffmpeginc=$row['_FFmpeg_included'];

        }
         echo "<tr id=\"".$rad."\" onclick=\"comment(this.id)\"><td>".$row['_Operating_system']."</td><td>".$row['_bit']."</td><td>".$row['_Python']."</td><td>".$row['_Qt']."</td><td>".$row['_Compiler']."</td><td>".$row['_Compiler_version']."</td><td>".$row['_GLIBC']."</td><td>".$row['_streamCapture2']."</td><td>".$row['_File_type']."</td><td>".$ffmpeginc."</td><td><a href=\"".$row['_Download_Link']."\">Ladda ner</a></td><td><a href=\"".$row['_MD5_and_Size']."\" target=\"_blank\">SHA256 och Storlek</a></td></tr>";

         $rad++;
    }
    echo "</table></div>";
    echo "</div>";
   
   
 

} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
  $conn = null;

?>

<script>
 let comments = <?php echo json_encode($comment); ?>
</script>


</div>
<footer>
        <div>&copy; Copyright 2016-<?php echo date("Y"); ?>
            Ingemar Ceicer<br>
            programming@ceicer.com
        </div>
    </footer>


</body>
</html>

