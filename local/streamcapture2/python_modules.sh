# Copyright 2022 - 2023 Ingemar Ceicer
# programming@ceicer.com
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#!/bin/bash

BOLD=$(tput bold)
NORMAL=$(tput sgr0)

echo -e ${BOLD}Checking python3...${NORMAL}
test=$([[ "$(python3 --version)" =~ "Python 3" ]] && echo "Python 3 is installed")
n=${#test}
if [ $n -eq 0 ]; then
    echo installing Python 3...
    sudo apt install python3
else
    test=$(python3 --version)
    echo $test is already installed
fi

echo
echo -e ${BOLD}Checking pip3...${NORMAL}
test=$(pip3 --version)
n=${#test}
if [ $n -eq 0 ]; then
    echo installing pip3...
    sudo apt install python3-pip
else
    echo $test is already installed
fi

echo
echo -e ${BOLD}Checking cryptography...${NORMAL}
test=$(pip3 freeze | grep -w cryptography)
n=${#test}

if [ $n -eq 0 ]; then
    echo installing cryptography...
    pip3 install cryptography
else
    echo cryptography is already installed
fi

echo
echo -e ${BOLD}Checking requests...${NORMAL}
test=$(pip freeze | grep -w requests)
n=${#test}

if [ $n -eq 0 ]; then
    echo installing requests...
    pip3 install requests
else
    echo requests is already installed
fi

echo
echo -e ${BOLD}Checking pysocks...${NORMAL}
test=$(pip freeze | grep -w PySocks)
n=${#test}

if [ $n -eq 0 ]; then
    echo installing pysocks...
    pip3 install pysocks
else
    echo pysocks is already installed
fi

echo
echo -e ${BOLD}Checking pyyaml...${NORMAL}
test=$(pip3 freeze | grep -w PyYAML)
n=${#test}

if [ $n -eq 0 ]; then
    echo installing pyyaml...
    pip3 install pyyaml

else
    echo pyyaml is already installed
fi
